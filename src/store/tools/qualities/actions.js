import { all as pullQualities } from 'plugins/firebase/tools/qualities/get'

export const PULL = async ({ commit }) => {
  const x = await pullQualities()
  if (x instanceof Array) commit('STORE', x)
}
