export const GET_QUALITIES = ({ qualities }) => {
  return qualities
}

export const GET_QUALITY_NAME = ({ qualities }) => (uid) => {
  return qualities.length && qualities.find(
    skill => skill.uid === uid
  ).name
}
