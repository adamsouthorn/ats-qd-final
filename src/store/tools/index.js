import lifeskills from './lifeskills'
import qualities from './qualities'
import areasOfLife from './areas-of-life'

export default {
  namespaced: true,
  modules: {
    lifeskills,
    qualities,
    areasOfLife
  }
}
