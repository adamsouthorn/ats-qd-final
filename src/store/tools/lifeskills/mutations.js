import Vue from 'vue'
const reduceData = (data) => {
  return data.reduce((map, l) => {
    const { uid, ...props } = l
    return {
      [uid]: props,
      ...map
    }
  }, {})
}

const findIndexBySkillId = (state, skillId) => state.lifeskills.findIndex(skill => skill && skill.uid === skillId)

export const STORE = (state, lifeskills) => {
  state.lifeskills = lifeskills
}

export const STORE_SKILLS_CATEGORIES = (state, data) => {
  const reveresedCategories = data.reverse()
  state.categories = reduceData(reveresedCategories)
}

export const CHANGE_SKILL_STATUS = (state, {skillId, status}) => {
  const index = findIndexBySkillId(state, skillId)
  let skill = state.lifeskills[index]
  Vue.set(skill, 'status', status)
}

export const CHANGE_SKILL_WAS_LIVE_FLAG = (state, {skillId}) => {
  const index = findIndexBySkillId(state, skillId)
  let skill = state.lifeskills[index]
  Vue.set(skill, 'wasLive', true)
}

export const CHANGE_SKILL_DATA = (state, {skillId, skillData}) => {
  const index = findIndexBySkillId(state, skillId)
  let skill = state.lifeskills[index]
  Vue.set(skill, 'skill', {...skill.skill, ...skillData})
}

export const REMOVE_SKILL_FROM_STORE = (state, {skillId}) => {
  const index = findIndexBySkillId(state, skillId)
  state.lifeskills.splice(index, 1)
}

export const STORE_SKILL = (state, {newSkillIndex, newSkillObject}) => {
  Vue.set(state.lifeskills, newSkillIndex, newSkillObject)
}
