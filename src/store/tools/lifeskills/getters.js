const FIELDS = {
  TITLE: 'title',
  DESCRIPTION: 'description'
}

export const UIDS_TO_NAMES = ({ lifeskills }) => (uids) => {
  // Unwrap UIDS
  if (!(uids instanceof Array)) {
    uids = UNWRAP_UIDS(uids)
  }

  // Maps Name to each UID
  if (uids instanceof Array) {
    return UIDS_TO_FIELD({
      field: FIELDS.TITLE,
      lifeskills,
      uids
    })
  } else {
    return []
  }
}

const UNWRAP_UIDS = (uids) => Object.keys(uids).filter(
  uid => uids[uid] === true
)

const UIDS_TO_FIELD = ({
  lifeskills,
  uids,
  field
}) => uids.map(
  uid => lifeskills && lifeskills[uid]
    ? lifeskills[uid][field] || 'Unknown Field!'
    : 'Unknown Soft Skill'
)

export const GET_CATEGORIES = ({ categories }) => {
  return categories
}

export const GET_LIFE_SKILLS = ({ lifeskills }) => {
  return lifeskills
}

export const GET_LIFE_SKILLS_FLATTENED = ({ lifeskills }) => {
  return lifeskills && lifeskills.map((lifeSkill) => {
    let skillData = lifeSkill.skill
    return {
      ...skillData,
      uid: lifeSkill.uid,
      status: lifeSkill.status,
      wasLive: lifeSkill.wasLive
    }
  })
}

export const GET_LIFE_SKILL_NAME = ({ lifeskills }) => (uid) => {
  return lifeskills.length && lifeskills.find(
    skill => skill.uid === uid
  ).skill.title
}
