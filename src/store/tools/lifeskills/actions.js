import { all as pullLifeSkills, categories as pullLifeSkillsCategories } from 'plugins/firebase/tools/lifeskills/get'
import { setSkillStatus, setSkillData, setSkillWasLiveFlag, deleteSkill, addNewSkill } from 'plugins/firebase/tools/lifeskills/set'
import { STORE_SKILLS_CATEGORIES, CHANGE_SKILL_STATUS, CHANGE_SKILL_WAS_LIVE_FLAG, CHANGE_SKILL_DATA, REMOVE_SKILL_FROM_STORE, STORE_SKILL } from './mutation.types'
import LifeSkillStatus from 'shared/LifeSkillStatus.js'

export const PULL = async ({ commit }) => {
  const x = await pullLifeSkills()
  if (x instanceof Array) commit('STORE', x)
}

export const PULL_SKILLS_CATEGORIES = async ({ commit }) => {
  const x = await pullLifeSkillsCategories()
  commit(STORE_SKILLS_CATEGORIES, x)
}

export const SET_WAS_LIFE_FLAG = async ({ commit }, { skillId }) => {
  await setSkillWasLiveFlag(skillId)
  commit(CHANGE_SKILL_WAS_LIVE_FLAG, { skillId })
}

export const SET_SKILL_STATUS = async ({ commit, dispatch }, { skillId, skillStatus }) => {
  if (skillStatus === LifeSkillStatus.Live) {
    await SET_WAS_LIFE_FLAG({ commit }, { skillId })
  }
  await setSkillStatus(skillId, skillStatus)
  commit(CHANGE_SKILL_STATUS, { skillId, status: skillStatus })
}

export const SET_SKILL_DATA = async ({ commit }, { skillId, skillData }) => {
  await setSkillData(skillId, skillData)
  commit(CHANGE_SKILL_DATA, { skillId, skillData })
}

export const DELETE_SKILL = async ({ commit }, skillId) => {
  await deleteSkill(skillId)
  commit(REMOVE_SKILL_FROM_STORE, { skillId })
}

export const ADD_NEW_SKILL = async ({ commit }, { skillStatus, skillData }) => {
  const { newSkillIndex, newSkillObject } = await addNewSkill(skillStatus, skillData)
  commit(STORE_SKILL, {
    newSkillIndex,
    newSkillObject
  })
}
