import AreasUI from 'shared/areas-of-life-ui'

export const ALL = ({ areasOfLife }) => {
  return areasOfLife
}

export const ALL_WITH_UI = ({ areasOfLife }) => {
  return Object.keys(areasOfLife).reduce((allUids, uid) => {
    allUids[uid] = {
      ...areasOfLife[uid],
      ...AreasUI[uid]
    }
    return allUids
  }, {})
}

export const UIDS = ({ areasOfLife }) => {
  return Object.keys(areasOfLife)
}

export const PRIORITY_UIDS = ({ areasOfLife }) => (areaUID) => {
  return Object.keys(areasOfLife[areaUID].priorities)
}

export const PRIORITY_UID_PATHS = ({ areasOfLife }) => Object.keys(areasOfLife).reduce((paths, areaUID) => {
  const area = areasOfLife[areaUID]
  return [
    ...paths,
    ...Object.keys(area.priorities).map(priority => ({
      path: `${areaUID}.${priority}`,
      label: area.priorities[priority].title,
      area: area.title,
      color: AreasUI[areaUID].color
    }))
  ]
}, [])

export const GET_PRIORITY_NAME = ({ areasOfLife }) => (uid) => {
  const segmentList = PRIORITY_UID_PATHS({ areasOfLife })
  return segmentList.length && segmentList.find(segment => segment.path === uid).label
}

export const GET_AREAS_OF_LIFE = ({ areasOfLife }) => {
  return Object.values(areasOfLife)
}
