import { all as pullAreas } from 'plugins/firebase/tools/areas-of-life/get'

export const PULL = async ({ commit }) => {
  const x = await pullAreas()
  commit('STORE', x)
}
