import Vue from 'vue'
import Vuex from 'vuex'

// we first import the module
import tools from './tools'
import authors from './authors'
import content from './content'
import payments from './payments'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    tools,
    authors,
    content,
    payments
  }
})

export default store
