import { getDataValue } from 'plugins/firebase/general'
import * as mutationTypes from './mutation.types'

export const PULL_PLANS = async ({ commit }) => {
  const value = await getDataValue('v1/stripe', 'plans')

  commit(mutationTypes.STORE_PLANS, value)
}
