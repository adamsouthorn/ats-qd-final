import { sortArrayOfObjectsByProp } from 'helpers/common'

export const GET_PLANS = state => state.plans

export const GET_PLANS_SELECT_OPTIONS = state => {
  if (!state.plans) return null

  const list = Object.entries(state.plans).map((obj) => {
    return {
      label: obj[1].nickname,
      value: obj[0]
    }
  })

  return sortArrayOfObjectsByProp(list, 'label')
}
