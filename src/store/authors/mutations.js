import Vue from 'vue'

export const STORE_AUTHOR = (state, {authorId, authorData}) => {
  Vue.set(state.authors, authorId, authorData)
  state.authors[authorId] = authorData
}
