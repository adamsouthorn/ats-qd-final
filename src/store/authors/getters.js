export const GET_AUTHOR_DATA = (state) => (authorId) => {
  return state.authors[authorId]
}
