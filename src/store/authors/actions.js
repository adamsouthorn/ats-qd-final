import { firebase } from 'plugins/firebase'
import { STORE_AUTHOR } from './mutation.types'

export const PULL_AUTHOR_DATA = async ({ commit }, authorId) => {
  const authorData = (
    await firebase.users.authorList
      .child(authorId)
      .once('value')
  ).val()
  let authorProfile = {}
  if (authorData) {
    const { email, profile } = authorData
    authorProfile = {
      ...profile,
      email
    }
  }
  commit(
    STORE_AUTHOR,
    {
      authorId,
      authorData: (({ displayName, avatar, email }) => ({
        displayName,
        authorId,
        email,
        avatar: avatar && (avatar.originalFileUrl || avatar.fileUrl)
      }))(authorProfile)
    })
}
