export const getResettableStateItems = () => ({
  term: null,
  filters: null,
  status: null,
  pagination: {
    rowsNumber: null,
    rowsPerPage: null,
    page: null,
    descending: null,
    sortBy: null
  },
  occuringStatuses: []
})

export default getResettableStateItems()
