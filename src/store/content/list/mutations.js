import { getResettableStateItems } from './state'

export const STORE_SEARCH_TERM = (state, term) => {
  state.term = term && term.trim()
}

export const STORE_FILTERS = (state, filters) => {
  state.filters = filters
}

export const STORE_PAGINATION = (state, {
  sortBy,
  descending,
  rowsNumber,
  rowsPerPage,
  page
}) => {
  state.pagination.sortBy = sortBy
  state.pagination.descending = descending
  state.pagination.rowsPerPage = rowsPerPage
  state.pagination.rowsNumber = rowsNumber
  state.pagination.page = page
}

export const STORE_STATUS = (state, status) => {
  state.status = status
}

export const STORE_AVAILABLE_STATUSES = (state, articles) => {
  state.occuringStatuses = articles && [...new Set(articles.map(article => article.status))]
}

export const RESET = (state) => {
  const stateItemsToReset = getResettableStateItems()
  Object.keys(stateItemsToReset).forEach(stateItem => {
    state[stateItem] = stateItemsToReset[stateItem]
  })
}
