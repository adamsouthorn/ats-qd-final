import {
  STORE_STATUS,
  STORE_SEARCH_TERM,
  STORE_FILTERS,
  STORE_PAGINATION,
  STORE_AVAILABLE_STATUSES,
  RESET as RESET_MUTATION
} from './mutation.types'

export const SET_STATUS = ({ commit }, status) => commit(STORE_STATUS, status)
export const SET_SEARCH_TERM = ({ commit }, term) => commit(STORE_SEARCH_TERM, term)
export const SET_FILTERS = ({ commit }, filters) => commit(STORE_FILTERS, filters)
export const SET_PAGINATION = ({ commit }, pagination) => commit(STORE_PAGINATION, pagination)
export const SET_AVAILABLE_STATUSES = ({ commit }, content) => commit(STORE_AVAILABLE_STATUSES, content)

export const RESET = ({ commit }) => {
  commit(RESET_MUTATION)
}
