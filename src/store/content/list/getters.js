export const GET_FILTERS = ({ filters }) => filters
export const GET_PAGINATION = ({ pagination }) => pagination
export const GET_SEARCH_TERM = ({ term }) => term
export const GET_STATUS = ({ status }) => status
export const GET_AVAILABLE_STATUSES = ({ occuringStatuses }) => occuringStatuses || []
