
import likes from './likes'
import list from './list'

export default {
  namespaced: true,
  modules: {
    likes,
    list
  }
}
