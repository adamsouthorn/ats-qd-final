import {
  listenToArticleUsefulness,
  unlistenToArticleUsefulness
} from 'plugins/firebase/content/article/useful'
import { STORE_USEFULNESS_SCORE } from './mutation.types'

export const LISTEN_TO_LIKES = ({ commit }, { uid, componentId }) => {
  listenToArticleUsefulness({ uid, componentId }, usefulnessScore => {
    commit(STORE_USEFULNESS_SCORE, { usefulnessScore, uid })
  })
}

export const UNLISTEN_TO_LIKES = ({ commit }, uid) => {
  unlistenToArticleUsefulness(uid)
}
