import Vue from 'vue'

export const STORE_USEFULNESS_SCORE = (state, {
  uid,
  usefulnessScore
}) => {
  Vue.set(state, uid, usefulnessScore)
}
