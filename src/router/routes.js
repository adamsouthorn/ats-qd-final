
import Permission from '../shared/access-control/Permission'

export default [
  {
    path: '/',
    name: 'root',
    component: () => import('layouts/default'),
    meta: { requiresAuth: true, permissionRequired: Permission.AccessPortal },
    children: [
      { path: '/getting-started', component: () => import('pages/getting-started'), meta: { permissionRequired: Permission.AccessContent } },
      { path: '/style-guide', component: () => import('pages/style-guide'), meta: { permissionRequired: Permission.AccessContent } },
      { path: '/tagging-toolkit', component: () => import('pages/tagging-toolkit/tagging-toolkit'), meta: { permissionRequired: Permission.AccessContent } },
      { path: '/blog', component: () => import('pages/blog'), meta: { permissionRequired: Permission.AccessBlogs } },
      { path: '/marketing', component: () => import('pages/marketing'), meta: { permissionRequired: Permission.AccessMarketing } },
      { path: '/users', component: () => import('pages/users'), meta: { permissionRequired: Permission.ListUsers } },
      { path: '/users-update', component: () => import('pages/users-update'), meta: { permissionRequired: Permission.UpgradeUsers } },
      { path: '/users/new', component: () => import('pages/user-create'), meta: { permissionRequired: Permission.ListUsers } },
      { path: '/users/me', component: () => import('pages/user'), props: { viewProfileOfCurrentUser: true } },
      { path: '/users/:id', component: () => import('pages/user'), props: true },
      { path: '/content-providers', component: () => import('pages/users/content-providers'), meta: { permissionRequired: Permission.ListAuthors } },
      { path: '/applicants', component: () => import('pages/users/applicants'), meta: { permissionRequired: Permission.ListAuthors } },
      { path: '/content', component: () => import('pages/contentList'), name: 'content', meta: { permissionRequired: Permission.AccessArticles } },
      { path: '/approval-queue', component: () => import('pages/approval-queue'), meta: { permissionRequired: Permission.ViewContentStats } },
      { path: '/content-stats', component: () => import('pages/content-stats'), meta: { permissionRequired: Permission.ViewContentStats } },
      { path: '/referral-stats', component: () => import('pages/statistics/referrals'), meta: { permissionRequired: Permission.ViewReferralStats } },
      { path: '/tools/life-skills', component: () => import('pages/tools/life-skills'), meta: { permissionRequired: Permission.ManageLifeSkills } },
      {
        path: '/content/:id',
        component: () => import('components/content-panel'),
        children: [
          {
            path: 'edit',
            name: 'edit',
            component: () => import('pages/content-edit'),
            meta: { permissionRequired: Permission.CreateArticles }
          },
          { path: 'preview', component: () => import('pages/content-preview'), props: { mobile: true } },
          { path: 'preview-tablet', component: () => import('pages/content-preview'), props: { tablet: true } },
          { path: 'preview-desktop', component: () => import('pages/content-preview'), props: { desktop: true } },
          { path: '/', redirect: '/content/:id/edit' }
        ]
      },
      {
        path: '/blog/:id',
        component: () => import('components/content-panel'),
        props: { blog: true },
        children: [
          {
            path: 'edit',
            name: 'blog-edit',
            component: () => import('pages/blog-edit'),
            meta: { permissionRequired: Permission.CreateBlogs }
          },
          { path: '/', redirect: '/blog/:id/edit' }
        ]
      }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/auth'),
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import('pages/auth/login')
      },
      {
        path: 'signup',
        name: 'signup',
        component: () => import('pages/auth/signup'),
        meta: { signup: true }
      },
      {
        path: 'forgot-password',
        name: 'forgot-password',
        component: () => import('pages/auth/forgot-password')
      }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
