import { intercom } from 'plugins/intercom'

export default async (to, from) => {
  intercom.notifyOfPageChange()
}
