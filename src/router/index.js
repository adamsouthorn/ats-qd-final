import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import { firebase } from '../plugins/firebase'
import accessControl from '../plugins/firebase/access-control'
import Permission from '../shared/access-control/Permission'
import { Dialog } from 'quasar'
import IntercomHook from './intercom.hook'

Vue.use(VueRouter)

const Router = new VueRouter({
  /*
   * NOTE! Change Vue Router mode from quasar.conf.js -> build -> vueRouterMode
   *
   * When going with "history" mode, please also make sure "build.publicPath"
   * is set to something other than an empty string.
   * Example: '/' instead of ''
   */

  // Leave as is and change from quasar.conf.js instead!
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE,
  scrollBehavior (to, from, savedPosition) {
    to.meta.fromHistory = savedPosition !== null
    return { y: 0 }
  },
  routes
})

Router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const user = await firebase.auth.getCurrentUser()

    let isLoggedIn = user && !user.isAnonymous
    if (!isLoggedIn) {
      return next({ name: 'login' })
    }

    if (to.matched.some((obj) => obj.path.includes('content')) && to.params.id) {
      const document = await firebase.content.articles('articles').get(to.params.id).document
      const docSnapshot = await document.get()
      const data = docSnapshot.data()
      let allowedToAccess = accessControl.userAllowedTo(Permission.ListArticles)
      if (!allowedToAccess && data && data.owner !== user.uid) {
        return next({ name: 'content' })
      }
    }
  }

  let isRootPath = to.name === 'root'
  if (isRootPath) {
    let allowedToAccessContent = await accessControl.fetchRolesAndCheckIfUserAllowedTo(Permission.AccessArticles)
    if (!allowedToAccessContent && !accessControl.userAllowedTo(Permission.AccessPortal)) {
      Dialog.create({
        title: 'Access denied',
        message: 'Current user does not have necessary permissions to access this page'
      })
      return next({ name: 'login' })
    }
    let redirectTo = allowedToAccessContent ? '/content' : '/users/me'
    return next(redirectTo)
  }

  let protectedRoute = [ ...to.matched ].reverse().find(record => record.meta.permissionRequired)
  if (protectedRoute) {
    let permissionRequired = protectedRoute.meta.permissionRequired
    let userAuthorisedToAccessPage = await accessControl.fetchRolesAndCheckIfUserAllowedTo(permissionRequired)
    if (!userAuthorisedToAccessPage) {
      return next('/')
    }
  }
  next()
})

Router.afterEach(IntercomHook)

export default Router
