const validationsMixin = {
  methods: {
    errorMessage (prefix, item) {
      if (!item.$error) return ''
      if (!item.required && item.$params.required) return `${prefix} is required`
      if (!item.minLength && item.$params.minLength) return `${prefix} must be at least ${item.$params.minLength.min} characters long`
      if (!item.maxLength && item.$params.maxLength) return `${prefix} must not be more than ${item.$params.maxLength.max} characters long`
      if (!item.minValue && item.$params.minValue) return `${prefix} must be at least ${item.$params.minValue.min}`
      if (!item.maxValue && item.$params.maxValue) return `${prefix} must not be more than ${item.$params.maxValue.max}`
      if (!item.mustContainNumber && item.$params.mustContainNumber) return `${prefix} must contain a number`
      if (!item.mustContainUppercase && item.$params.mustContainUppercase) return `${prefix} must contain an uppercase letter`

      return ''
    }
  }
}

export default validationsMixin
