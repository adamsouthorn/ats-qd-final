
import 'quill/dist/quill.snow.css'
import _Quill from 'quill'
import Parchment from 'parchment'

const Quill = window.Quill || _Quill

const setCustomHeadingSizes = quillInstance => {
  var fontSizeStyle = Quill.import('attributors/class/size')
  fontSizeStyle.whitelist = [false, 'h1', 'h2', 'h3', 'subhead', 'small']
  Quill.register(fontSizeStyle, true)
}

const setFormatClass = (format, className) => {
  let Format = Quill.import(`formats/${format}`)
  Format.className = className
  Parchment.register(Format)
}

setFormatClass('link', 'link')
setFormatClass('list', 'list')
setCustomHeadingSizes()

const defaultConfig = {
  theme: 'snow',
  boundary: document.body
}

const addUrlValidation = quillInstance => {
  if (!quillInstance.theme.tooltip) {
    return
  }
  let saveLinkFn = quillInstance.theme.tooltip.save
  quillInstance.theme.tooltip.save = function () {
    var url = this.textbox.value
    let validUrl = url.startsWith('https://') || url.startsWith('http://')
    if (validUrl) {
      saveLinkFn.call(this)
    } else {
      alert('Please enter a valid link starting with http:// or https://')
    }
  }
}

export default function CustomQuill (element, config) {
  let quillInstance = new Quill(element, { ...defaultConfig, ...config })
  addUrlValidation(quillInstance)
  return quillInstance
}
