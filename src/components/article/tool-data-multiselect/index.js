import lifeSkillsMultiSelect from './life-skills-multiselect'
import qualitiesMultiSelect from './qualities-multiselect'

export {
  lifeSkillsMultiSelect,
  qualitiesMultiSelect
}
