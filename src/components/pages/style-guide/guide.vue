<template>
  <div>
    <div class="introduction introduction--image">
      <img class="intro-image" src="~assets/images/laptop.png"/>
      <text-block class="intro-text">
        <h1>QDooz content style guide</h1>
        <p class="title">Please read through the entire style guide to ensure the structure, style and tone of your content is suitable for QDooz.</p>
      </text-block>
    </div>
    <div class="guide">
      <table-of-contents :links="contents" />
      <page-contents>
        <text-block importance="minor" :padding="{v: true, h: false}">
          <h1 class="highlighted">Editors’ notes</h1>
          <a ref="editor-notes" />
          <p>There are plenty of places people can go to seek professional and personal development. Professional Membership Organisations (PMOs), businesses and individuals alike have a lot of choice in training providers and inspirational guidance on self-improvement across all aspects of their lives. So, what makes QDooz unique?</p>
          <p>You.</p>
          <p>Your life experiences and your voice are what helps to make us different. You are going to be the coach and the mentor in our Member’s pocket. They will turn to you when directed by a line manager, and also because they are driven to constantly learn and improve. They are seeking a place of warmth and wisdom.</p>
          <p>Inspire curiosity with your title. Sell your article in 50 characters. Then, use an inspirational quotation or a powerful summary in your introduction. Take some time to draw your reader further into your article in the 140 characters allocated.</p>
          <p>Your reader is now with you. They have committed. You can start to give them the details in the body of your article: present the essence of your argument. Use case studies, anecdotes and real examples to illustrate your point. Talk to them directly about how your experience and wisdom may be relevant to their lives.</p>
          <p>Then, pause. You are about to write your action points. If you are the USP of our brand, then the action points are the USP of our accreditation. How will the reader continue personal reflection and development to embed the learning outcome pursued in your article?</p>
          <p>No one enjoys being told what to do. We all have a toddler in our brain resisting instructions. So, when writing these vital action points, use Socratic, open-ended questions. Encourage them to make the links between your stories and ideas and the lives they are leading. Then, you may want to suggest they reflect on what they have learnt, write in their journal, explore the ideas with a friend, or apply the conclusions in work, or some other valid onward experience that will deepen the learning, and heighten their sense of engagement, achievement and reward.</p>
          <p>Finally, carefully proofread and edit your work, so your ideas are never lost in bafflement at a typo.</p>
          <p>
            The editorial team is also here to support you in your work. We are writers with QDooz too. We can offer inspiration, guidance, crafting advice and support when needed. Email us at
            <link-to
              url="mailto:editorial@qdooz.com"
              text="editorial@qdooz.com"
              :icon="false"
              external
              newWindow
            />
            with any questions or requests.</p>
        </text-block>
        <text-block importance="minor" :padding="{v: true, h: false}">
          <h1 class="highlighted">1. Content on QDooz</h1>
          <a ref="content" />
          <p>Content on QDooz is tagged with Areas of Life, Soft Skills and/or Qualities, which determine the <span class="bold">‘content focus’</span> of the piece.</p>
          <img class="content-image" src="~assets/images/style-table.png" width="711"/>
        </text-block>
        <text-block importance="minor" :padding="{v: true, h: false}">
          <h1 class="highlighted">2. Style and tone</h1>
          <a ref="style-and-tone" />
          <p class="heading">2.1. Target audience</p>
          <p>Our audience includes:</p>
          <ul>
            <li>Members of professional organisations doing Continuing Professional Development (CPD)</li>
            <li>Students looking to develop the skills necessary to thrive in today’s employment market</li>
            <li>Employers who recognise the importance of Soft Skills development</li>
            <li>Individuals who are lifelong learners</li>
          </ul>
          <p>Our content meets the requirements of the CPD Standards Office as it enables individuals to:</p>
          <ul>
            <li>Explore a new angle or perspective on an idea or concept</li>
            <li>Acquire deeper or extended knowledge as a result of engaging with your content</li>
            <li>Take away concrete actions to deepen and apply their learning</li>
            <li>Extend their learning by reflecting on the impact in their lives</li>
          </ul>
          <p class="heading">2.2. Areas of life</p>
          <p>QDooz’s four Areas of Life (My Self, My Work, My Relationships and My Lifestyle) guide our users to the most useful and important content for them. Each Area of Life has three subsections that further refine our tailored content offering.</p>
          <ul>
            <li><span class="bold">Do</span> think about how your content can have an impact on our Members’ lives and tag accordingly</li>
            <li><span class="bold">Don’t</span> just select everything!</li>
          </ul>
          <p class="heading">2.3. What to include in your content</p>
          <p>Content should be tagged with the most relevant Soft Skill. Please familiarise yourself with the descriptions of Soft Skills and tag accordingly.</p>
          <ul>
            <li><span class="bold">Length</span>: 500-750 words for the main body, or maximum 5 minutes (+10%) of video/audio.</li>
            <li><span class="bold">Title</span>: a punchy, eye-catching title of maximum 50 characters. Sentence case. No full stops.</li>
            <li><span class="bold">Introduction</span>: A short, attention-grabbing opening of maximum 150 characters. Avoid repetition of the title in the introduction, and of the introduction in the main body of text. Consider using a quote directly relevant to the Soft Skill selected.</li>
            <li><span class="bold">Tagging</span>: You will assign to each piece of content <span class="bold">One Soft Skill</span> and maximum <span class="bold">three Qualities</span> for a <span class="bold">Soft Skill piece; no Soft Skills</span> and <span class="bold">one Quality</span> for a <span class="bold">Quality piece;</span> at least one specific segment within one of the four Areas of Life.</li>
            <li><span class="bold">Actions</span>: Maximum <span class="bold">three</span> actionable things for the member ‘to do’ to improve their selected Soft Skill, 60-300 words.</li>
            <li><span class="bold">Image</span>: an eye-catching, relevant image to support your content <i>[See below]</i></li>
            <li><span class="bold">Suggested formats</span>:
              <ul>
                <li><span class="bold">Case studies</span>: Look at an example or story to inspire or advise members.</li>
                <li><span class="bold">Q&A</span>: Information presented in a Q&A/interview style format.</li>
                <li><span class="bold">Quote</span>: Quote a well-known person or short anecdote and tell a story around it.</li>
                <li><span class="bold">Personal reflections</span>: Draw on your career or life experience.</li>
                <li><span class="bold">Poetry and prose</span>: Tell a story, then use it to inspire and mentor.</li>
              </ul>
            </li>
          </ul>
          <p class="heading">2.4. Sources, citations and politics</p>
          <p>When discussing scientific research and findings, even on broadly accepted topics such as global warming, cite a reputable source (e.g. “a 2011 study by the University of Oxford found...”) or your own relevant career story (e.g. “as a doctor in a busy hospital, I found...”) in the text. <span class="bold">Take great care around modern politics, current affairs and other topics likely to spark controversy.</span></p>
        </text-block>
        <text-block importance="minor" :padding="{v: true, h: false}">
          <h1 class="highlighted">3. Format and grammar</h1>
          <a ref="format-and-grammar" />
          <p class="heading">3.1. Titles</p>
          <ul>
            <li><span class="bold">Capitalisation</span>: QDooz titles should be written in sentence case. Use capitals for each word if it is the title (or plays on the title) of a lyric, other work, or established concept that is capitalised.</li>
            <li><span class="bold">Punctuation</span>: Do not use full stops/periods at the end of titles.</li>
            <li><span class="bold">Style example</span>: ‘How To Develop Confidence.’ <span class="bold">is incorrect</span>. ‘How to develop confidence’ <span class="bold">is correct</span>.</li>
          </ul>
          <p class="heading">3.2. Text</p>
          <p>Pieces should be written in a concise, accessible style. Keep grammar and spelling consistent.</p>
          <ul>
            <li>
              <span class="bold">Bullet points</span>:
              <ul>
                <li><span class="bold">Don’t </span>use a full stop/period at the end of a bullet point unless it follows one or more full sentences.</li>
                <li><span class="bold">Don’t </span>use all-caps words and avoid over-capitalisation.</li>
              </ul>
            </li>
            <li><span class="bold">Names, ideas and titles</span>: Use single quotes (‘...‘) for concepts, expressions and colloquialisms, and italics for book, play and film titles. Capitalise names and proper nouns.</li>
            <li><span class="bold">Numbers</span>: Use words for numbers below 100 (e.g. seventy-three) and for colloquial or indefinite amounts (a million, two dozen). Separate numbers beyond 1,000 with a comma every three digits (4,567). Numbers at the start of a sentence should always be written.</li>
            <li><span class="bold">Punctuation</span>: Avoid overuse of ellipses (...); do not use the Oxford comma.</li>
            <li><span class="bold">Quotations</span>: Quote in double quotes (“...”), with paraphrasing between square brackets ([]).</li>
            <li><span class="bold">Review</span>: Use the ‘Preview’ function to check formatting.</li>
            <li><span class="bold">Spacing</span>: <span class="bold">Don’t </span> double space. <span class="bold">Do </span> press ‘Enter’ once to begin a new paragraph.</li>
            <li><span class="bold">Style</span>: General points to consider:</li>
            <ul>
              <li><span class="bold">Do </span>use <i>italics</i> if you wish to emphasise a point.</li>
              <li><span class="bold">Do </span>use <b>bold</b> for subheads.</li>
              <li><span class="bold">Don’t </span>capitalise the next word after a colon or semi-colon, unless you’re writing on a different line (as we are here!), a proper noun or referring to a title.</li>
              <li><span class="bold">Don’t </span><u>underline</u> as this could be confused for a hyperlink.</li>
            </ul>
          </ul>
          <p class="heading">3.3. Images</p>
          <p>All QDooz articles require a relevant and eye-catching image that you have permission to use and/or repurpose. You may refer to: <link-to text="Unsplash" url="https://unsplash.com/" newWindow external />, <link-to text="Pexels" url="https://www.pexels.com/" newWindow external /> and <link-to text="Pixabay" url="https://pixabay.com/" newWindow external />. QDooz has no affiliation with these websites, so please double check licensing before using each image.</p>
          <ul>
            <li><span class="bold">Do </span>use images that you’ve taken or made and/or are royalty free (for commercial use). You will grant the rights to your own images and text to QDooz when you submit a piece for publication.</li>
            <li><span class="bold">Do </span>feel free to draw a relevant, appropriate picture for your content.</li>
            <li><span class="bold">Do </span>upload images in greater than 796*453 resolution, though much higher is better!</li>
            <li><span class="bold">Don’t </span>overlay your image with text, a title or description of any kind.</li>
          </ul>
          <p class="heading">3.4. Videos</p>
          <p>The platform supports video in <span class="bold">.mp4</span> format. Use this format for podcasts and other audio content (with a title card).</p>
          <ul>
            <li><span class="bold">Do </span>ensure video is in at least 720p resolution (1280*720).</li>
            <li><span class="bold">Do </span>make your video file name [Your name][-][Article name] (e.g. ‘John Smith – Seeing emotion’).</li>
            <li><span class="bold">Do </span>film videos in landscape, in a well-lit room with minimal clutter.</li>
            <li><span class="bold">Do </span>upload video in the ‘Featured video’ slot.</li>
            <li><span class="bold">Do </span>add a quote or introduction in text below the video.</li>
            <li><span class="bold">Don’t </span>advertise or self-promote in your text, audio or video.</li>
            <li><span class="bold">Don’t </span>upload video longer than 5 minutes +/- 10%.</li>
            <li><span class="bold">Don’t </span>add captions or subtitles to your video – we’ll do that for you.</li>
            <li><span class="bold">Be careful </span>around images of proprietary characters (e.g. from film, television, cartoons, comics); for legal reasons these can be fraught territory – please contact the editorial team for guidance.</li>
          </ul>
          <p class="heading">3.5. ‘Things to do’</p>
          <p>‘Things to do’, also referred to as <span class="bold">‘action points’</span> or <span class="bold">‘actions’</span>, are at the heart of the QDooz approach to learning. Make sure yours are clear, precise and focused on the Quality or Soft Skill in question.</p>
          <ul>
            <li><span class="bold">Do use open questions to encourage reflection.</span></li>
            <li><span class="bold">Do</span> ensure action points are 60-300 words in length (in total).</li>
            <li><span class="bold">Do</span> ensure each of the action points goes in a separate box.</li>
            <li><span class="bold">Do</span> end each of the action points in a full stop/period or question mark.</li>
            <li><span class="bold">Don't</span> number or bullet the action points.</li>
          </ul>
        </text-block>
        <text-block importance="minor" :padding="{v: true, h: false}">
          <h1 class="highlighted">4. Content Policy</h1>
          <a ref="content-policy" />
          <p><span class="bold">Promotion and advertising</span>: QDooz will <span class="bold">not</span> approve content promoting a product or service, nor will content be published in which self-promotion is deemed overt or excessive. If Members like your content, they’ll visit your profile page, follow you, and reach out to you directly to explore your products and services.</p>
          <ul>
            <li>
              <p><span class="bold">Do</span> add a link to your personal or professional website <span class="bold">in your profile</span></p></li>
            <li>
              <span class="bold">Don't</span> ask Members (in either your text/video or ‘Things to do’) to:
              <ul>
                <li>Sign up to a mailing list (even if it is free)</li>
                <li>Purchase or download anything (e.g. a course or a book)</li>
              </ul>
            </li>
          </ul>
          <p>QDooz reserves the right to edit, modify, add to and remove the pieces you upload to the Content Portal if they do not meet our quality standards, learning criteria or contain language we deem, in our sole editorial view, is unclear or unacceptable.</p>
        </text-block>
        <text-block importance="minor" :padding="{v: true, h: false}">
          <h1 class="highlighted">5. Hints and tips</h1>
          <a ref="hints-and-tips" />
          <p>Provide members with content in different formats (articles, videos, listicles, audios) and styles (dialogues, case studies, stories, metaphors, poems, analogies, self-assessment tools) – the more variety the better! Please consider alternative options – especially short videos! (See <article-link id="57RFJ9EbRcM9Uf5PwFvo">Robin Miles’ video on “How to be more assertive”</article-link>.)</p>
          <p class="heading">5.1. Text entry</p>
          <p>Write your article in Word or Pages (or Google Docs), use spell-check, and then copy and paste it into the Portal text boxes, instead of typing directly into them. It may benefit you to use a grammar-checking add-on, like Grammarly (with which QDooz has no affiliation).</p>
          <p class="heading">5.2. Example pieces</p>
          <p>It may be helpful to have some examples of content that has performed well with test users so far:</p>
          <ul>
            <li><span class="bold">‘Things to do’ that encourage deep reflection</span> thought and personal practice that reaches to the core of shared human experience (see <article-link id="7XhJV6VZ3MXeXh47xNWs">Racheal Smith’s "The strongest struggle"</article-link>)</li>
            <li><span class="bold">Personal stories, anecdotes and case studies</span> (see <article-link id="ynXRI7JQ4O676f7c5iQa">Pat Cleverly's "Master potter”</article-link>) particularly for content on Qualities.</li>
            <li><span class="bold">Unexpected perspectives or more unusual examples</span> e.g. De Bono’s lateral thinking problems are valuable, however, these are well-known. Is there an exercise in lateral thinking that you have developed or is more unusual? (see <article-link id="Cz0g95n7ypzCh5bvxYK1">Amy Hackett-Jones' "A tale of two pebbles"</article-link>.)</li>
            <li><span class="bold">Pieces that use historical examples</span> to ask questions about Members’ present day lives (see <article-link id="m7ngiiTLFNC9qVbLUnew">Danielle Grant’s "Nelson Mandela: a case study for forgiveness"</article-link>)</li>
            <li><span class="bold">Video that is concise and moreish</span> that challenges the viewer and maintains their interest (see <article-link id="PFrzoKN9RB2d6YCPFQoA">Cat Duval’s "Dealing with information overload"</article-link>)</li>
            <li><span class="bold">Personal experience of difficult circumstances</span> from which readers can learn (see <article-link id="DE13aLxOQu7e2AkMLMQf">Racheal Smith’s "My enemy as my teacher"</article-link>)</li>
            <li><span class="bold">Applying the Quality or Soft Skill in different contexts </span>e.g. for negotiation – how can this be applied to asking for a pay rise, negotiating with your children/family, buying a car …even world peace? (see <article-link id="svRvVE8pnZK0CBL4O6ts">Danielle Grant's "Getting paid what you are worth"</article-link>.)</li>
            <li><span class="bold">Pieces that include scientific research, and which cite relevant sources</span> (see <article-link id="Z9NvRKt3kBWD3TKrIx4f">John Knights' "Fair: treating equally and without bias"</article-link>.)</li>
            <li><span class="bold">Interview-style pieces</span>, especially those with a light-hearted or jovial style (see <article-link id="anzUs8RrozVC9JVAA4At">Chris Godfrey’s "Taking Sherlock Holmes in for questioning"</article-link>.)</li>
            <li><span class="bold">A human touch! Content that has a unique tone, that uses humour</span> (see <article-link id="J7KmDk74xCxeWJBoPa4b">Chris Godfrey's "Game on – social compliance is the only way to win"</article-link>.)</li>
            <li><span class="bold">Stories that resonate emotionally</span> (see <article-link id="88nraYCKAR1KoGYb2IbB">Sarai Lewis’ "My grief is not the same as yours"</article-link>.)</li>
            <li><span class="bold">A pithy, expressive poem</span> (see <article-link id="YjKwyNGPBRXoVuJfiH3m">Simon Barrow’s "Being ghosted"</article-link>.)</li>
            <li><span class="bold">A piece that engages with a deep reality</span> through humour, warmth and vulnerability (see <article-link id="GFEFOtkOYWdYYRgQMWRI">William Hackett-Jones' "Thinking things through to be more assertive"</article-link>.)</li>
          </ul>
          <p class="heading">5.3. Further video examples</p>
          <ul>
            <li>Violet Ryder’s <article-link id="Y2teCkhelubvcg94tyMd">"Developing sensitivity"</article-link> utilises the <span class="bold">podcast-style format</span> to great effect.</li>
            <li>Jim Hughes’ <article-link id="kcIc4grn2Hgd04Q4bQPl">"Be happier with less"</article-link> displays the <span class="bold">clarity, high-resolution and bright lighting</span> expected of QDooz video content.</li>
            <li>Sandra Crathern’s <article-link id="skzEAvKMUqPR4Kp1YjOJ">"Connection is the name of the game"</article-link> has the <span class="bold">friendly, warm tone</span> that resonates with our diverse viewership.</li>
          </ul>
          <p class="heading">5.4. Downloadable PDFs and links</p>
          <p>QDooz does not currently support downloadable PDFs or links. While you may refer to online resources, please do not attempt to use hyperlinks, or spell out URLs (web addresses) in your piece – they will not work, and will be removed by our editorial team.</p>
          <p class="heading">5.5. Jargon</p>
          <p>Content should be drawn from personal experience to ensure authenticity. If using jargon, include a simple explanation to aid understanding. For example, when talking about neuroplasticity, say ‘the ability of the brain to change throughout an individual's life as a result of our experiences and learning’. (See <article-link id="L3j9cESNqXK2tvG3Bqui">Danielle Grant's "Start with self-awareness"</article-link>.)</p>
          <p class="heading">5.6. Final checks</p>
          <p>Read through your work to ensure there are no typos and grammar errors lurking! Previewing your content in tablet and mobile formats will show you how it looks across QDooz’s range of platforms. Then go back to ‘edit’ mode and amend as necessary.</p>
        </text-block>
      </page-contents>
    </div>
  </div>
</template>

<style lang="stylus" scoped>
@import '~variables'
.bullet
  margin-left 20px
.introduction
  position relative
  color #202020
  padding $page-margin-minor $page-padding
  &.introduction--image .text-block
    border-bottom none
.intro-image
  width 100%
  height 100%
  position absolute
  object-fit cover
.intro-text
  z-index 1
  padding: 20px 0 90px
  width 80%
  margin-top 0
  @media screen and (min-width $breakpoint-lg)
    width 70%
    padding-bottom 120px
.guide
  position relative
  padding $page-content-padding-major-vertical $page-padding-right $page-content-padding-major-vertical $page-padding
.bold
  font-weight 600
.italics
  font-style italic
.title,
.subtitle
  font-weight 500
.guide .subtitle
  margin-bottom -1rem
.content-image
  max-width 100%
</style>

<script>
import tableOfContents from 'components/ui/table-of-contents/table-of-contents'
import pageContents from 'components/ui/table-of-contents/contents'
import textBlock from 'components/ui/text-block'
import linkTo from 'components/ui/link'
import { articleLink } from 'components/ui/links'

export default {
  components: {
    tableOfContents,
    pageContents,
    textBlock,
    linkTo,
    articleLink
  },
  mounted () {
    this.contents = [
      { label: 'Editors’ notes', anchor: this.getAnchor('editor-notes') },
      { label: 'Content on QDooz', anchor: this.getAnchor('content') },
      { label: 'Style and tone ', anchor: this.getAnchor('style-and-tone') },
      { label: 'Format and grammar', anchor: this.getAnchor('format-and-grammar') },
      { label: 'Content policy', anchor: this.getAnchor('content-policy') },
      { label: 'Hints and tips', anchor: this.getAnchor('hints-and-tips') }
    ]
  },
  data: () => {
    return {
      contents: []
    }
  },
  methods: {
    getAnchor (to) {
      return { to, el: this.$refs[to] }
    }
  }
}
</script>
