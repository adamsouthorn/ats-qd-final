export const config = {
  versions: {
    firebase: {
      database: {
        realtime: process.env.api.version,
        firestore: process.env.firestore.version
      },
      storage: process.env.storage.version
    }
  },
  api: {
    tools: `${process.env.api.version}/tools`,
    users: `${process.env.api.version}/users`,
    metadata: `${process.env.api.version}/metadata`,
    authors: `${process.env.api.version}/content/authors`,
    authorApplicants: `${process.env.api.version}/content/authorApplicants`,
    useful: `${process.env.api.version}/content/useful`
  },
  storage: {
    users: `/${process.env.storage.version}/users`,
    content: `/${process.env.storage.version}/content`
  },
  firestore: {
    version: `${process.env.firestore.version}`
  },
  cloudFunctions: {
    url: `${process.env.firebaseConfig.cloudFunctionsURL}/api`,
    getUser: `getUser`,
    getUserList: `getUserList`,
    getUsersInBulk: `getUsersInBulk`,
    findUserByEmail: `findUserByEmail`,
    setCustomUserClaims: `setCustomUserClaims`,
    updateUserDetails: `updateUserDetails`,
    sendAuthorApproveEmail: `sendAuthorApproveEmail`,
    sendAuthorRejectEmail: `sendAuthorRejectEmail`,
    onAdminArticleFeedbackEmail: `onAdminArticleFeedbackEmail`,
    getModerators: `getModerators`,
    onGenerateBulkVouchers: `onMarketingGenerateBulkVouchers`,
    onPostToSlack: `onPostToSlack`
  },
  wistia: {
    apiUrl: `${process.env.wistia.apiUrl}`,
    uploadUrl: `${process.env.wistia.uploadUrl}`,
    key: `${process.env.wistia.apiKey}`,
    projectId: `${process.env.wistia.projectId}`,
    uploadToken: `${process.env.wistia.uploadToken}`,
    videoJSONUrl: `${process.env.wistia.videoJSONUrl}`
  },
  intercom: {
    appId: `${process.env.intercom.appId}`
  },
  slack: {
    portalAppId: `${process.env.slack.portalAppId}`
  },
  cdnOptimizationServiceUrl: 'https://avpsadgken.cloudimg.io',
  webapp: process.env.webapp,
  sentry: {
    dsn: `${process.env.sentry.dsn}`,
    env: `${process.env.sentry.env}`
  }
}

export default config
