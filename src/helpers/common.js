import cloneDeep from 'lodash/cloneDeep'

/**
 * Sorts an array of objects by a specified property
 *
 * @param {Array} array
 * @param {String} prop
 * @param {String} sortOrder
 */
export const sortArrayOfObjectsByProp = (array, prop, sortOrder = 'asc') => {
  const cloneArray = cloneDeep(array)

  const asc = (a, b) => {
    if (a[prop] < b[prop]) {
      return -1
    }

    if (a[prop] > b[prop]) {
      return 1
    }

    return 0
  }

  const desc = (a, b) => {
    if (a[prop] > b[prop]) {
      return -1
    }

    if (a[prop] < b[prop]) {
      return 1
    }

    return 0
  }

  if (sortOrder === 'desc') {
    return cloneArray.sort(desc)
  }

  return cloneArray.sort(asc)
}

/**
 * Converts a string to a dashed seperated string
 *
 * @param {String} str
 */
export function toDashSeperated (str) {
  return str.replace(/\s/g, '-').toLowerCase()
}

/**
 * Checks if an array is multidemential
 *
 * @param {Array} array
 */
export const isMultidementialArray = (array) => {
  return (Array.isArray(array) && array.length > 0 && Array.isArray(array[0]))
}

/**
 * Converts an array into CSV
 *
 * @param {Array} array
 */
export const convertArrayToCSV = (array) => {
  if (!Array.isArray(array)) throw Error('Value must be an array')

  const csvPrefix = 'data:text/csv;charset=utf-8,'

  if (isMultidementialArray(array)) {
    return csvPrefix + array.map(element => element.join(',')).join('\n')
  }

  return csvPrefix + array.join('\n')
}

/**
 * Download a recieved CSV string as a named file
 *
 * @param {String} csv
 * @param {String} fileName
 */
export const downloadCSVFile = (csv, fileName) => {
  const encodedUri = encodeURI(csv)
  const link = document.createElement('a')

  link.setAttribute('href', encodedUri)
  link.setAttribute('download', `${fileName}.csv`)
  document.body.appendChild(link)

  link.click()

  setTimeout(() => {
    link.remove()
  }, 1000)
}
