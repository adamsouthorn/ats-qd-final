import firebase from 'firebase/app'
import config from './../config'

const SendToSlack = (text, appId) => {
  const dataPayload = {
    text,
    appId
  }
  const sendSlackEventCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.onPostToSlack
  )
  sendSlackEventCloudFunction(dataPayload)
}

export default SendToSlack
