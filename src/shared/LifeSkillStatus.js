const LifeSkillStatus = {
  Live: 'live',
  LiveSoon: 'live_soon',
  NotLive: 'not_live'
}
const statusUI = (label, color) => ({ label, color })

export const LifeSkillStatusUI = {
  [LifeSkillStatus.Live]: statusUI('Live', 'green'),
  [LifeSkillStatus.LiveSoon]: statusUI('Live Soon', 'orange'),
  [LifeSkillStatus.NotLive]: statusUI('Not Live', 'grey')
}

export default LifeSkillStatus
