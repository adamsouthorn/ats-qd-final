import { debounce } from 'quasar'
import { roleNameMap } from './../shared/access-control/UserRole.js'
import config from 'config'

export const inputDebounce = fn => debounce(fn, 700)
export const searchDebounce = fn => debounce(fn, 400)

export const arrayToMap = (array) => {
  return array.reduce((map, el) => {
    map[el] = true
    return map
  }, {})
}

export const mapToArray = (map) => {
  return map && Object.keys(map).filter(Boolean)
}

export const getNestedProperty = (obj, path, separator = '.') => {
  var properties = Array.isArray(path) ? path : path.split(separator)
  return properties.reduce((prev, curr) => prev && prev[curr], obj)
}

export const htmlToText = (html) => {
  const { body: { textContent: abstract = '' } } = new DOMParser().parseFromString(html, 'text/html')
  if (abstract === 'undefined') {
    return null
  }
  return abstract
}

export const rolesMapToString = (rolesMap) => {
  if (rolesMap) {
    let activeRoles = Object.keys(rolesMap)
      .filter(role => rolesMap[role])
    if (activeRoles.length > 0) {
      let activeRoleNames = activeRoles
        .map(role => {
          return roleNameMap[role]
        }).filter(role => {
          return role
        }).join(', ')
      return activeRoleNames
    }
  }
  return 'Basic User'
}

export const getLinkToArticlePreview = articleId => {
  const webappConf = config.webapp
  return `${webappConf.url}/${webappConf.preview}/${articleId}`
}
