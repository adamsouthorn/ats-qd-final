const AuthorProfileModel = {
  displayName: null,
  vocation: null,
  country: null,
  websiteUrl: null,
  bio: null,
  socialHandles: {
    twitter: null,
    facebook: null,
    instagram: null,
    linkedIn: null,
    companyLinkedIn: null
  },
  avatar: null,
  theme: null
}

export default AuthorProfileModel
