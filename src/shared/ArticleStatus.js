const Draft = 'draft'
const ReviewNeeded = 'review_needed'
const InReview = 'in_review'
const AmendmentsRequired = 'amendments_required'
const Rejected = 'rejected'
const Published = 'published'

const statusUI = (name, color, icon) => ({ name, color, icon })
const statusUpdate = (msg, dateField) => ({ msg, dateField })

const statusMap = {
  Draft,
  ReviewNeeded,
  InReview,
  AmendmentsRequired,
  Rejected,
  Published
}

export const statusList = Object.values(statusMap)

export const statusDisplayProperties = {
  [Draft]: statusUI('Draft', '#7C7C7C', 'edit'),
  [ReviewNeeded]: statusUI('Submitted for review', '#E89153', 'assignment'),
  [InReview]: statusUI('Under review', '#E89153', 'update'),
  [AmendmentsRequired]: statusUI('Action needed', '#f17160', 'speaker_notes'),
  [Rejected]: statusUI('Rejected', '#f17160', 'cancel'),
  [Published]: statusUI('Published', '#bfd264', 'check_circle')
}

export const statusUpdateDisplay = {
  [Draft]: statusUpdate('Created', 'createdTime'),
  [ReviewNeeded]: statusUpdate('Submitted for review', 'sentForReviewTime'),
  [InReview]: statusUpdate('Under review', 'inReviewTime'),
  [AmendmentsRequired]: statusUpdate('Action needed', 'amendmentsRequiredTime'),
  [Rejected]: statusUpdate('Rejected', 'rejectedTime'),
  [Published]: statusUpdate('Published', 'publishedTime')
}

export default statusMap
