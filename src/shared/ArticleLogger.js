import firebase from 'firebase/app'
import 'firebase/firestore'
import { config } from './../config'

const articleLogs = id => firebase.firestore().collection(
  `content/${config.firestore.version}/articlesLogs`
).doc(id).collection('logs')

const articleMeta = id => firebase.firestore().collection(
  `content/${config.firestore.version}/articlesMeta`
).doc(id)

export const LogArticleEvent = async ({ userId, status, date }, id) => {
  await articleLogs(id).doc().set({
    userId,
    status,
    date
  }, {
    merge: true
  })
  return { userId, id }
}

const LogArticleMeta = async (data, id) => {
  await articleMeta(id).set(data, {
    merge: true
  })
  return { data, id }
}

const GetArticleLogs = async (id) => {
  const logs = await articleLogs(id).orderBy('date', 'desc').get()
  return logs
}

const GetArticleMeta = async (id) => {
  const meta = await articleMeta(id).get()
  return meta
}

export default LogArticleEvent
export { LogArticleMeta, GetArticleLogs, GetArticleMeta }
