const genUid = (prefix, id) => `${prefix}${String(id).padStart(3, '0')}`
const AreaUid = (n) => genUid('ar', n)

export default {
  [AreaUid(1)]: {
    color: '#E89153'
  },
  [AreaUid(2)]: {
    color: '#609EAB'
  },
  [AreaUid(3)]: {
    color: '#C75E81'
  },
  [AreaUid(4)]: {
    color: '#B47EB9'
  }
}
