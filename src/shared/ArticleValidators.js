
import { required, helpers, maxLength } from 'vuelidate/lib/validators'
import { Quality } from 'shared/ArticleFocus'

const count = (data) => Object.keys(data).map(
  auid => Object.keys(
    data[auid]
  ).reduce(
    (count, puid) => data[auid][puid]
      ? count + 1
      : count,
    0
  )
).reduce(
  (count, auid) => auid + count,
  0
)

const selected = (data) => (data instanceof Object) && count(data) > 0
const maxTagsSelected = (tags, max) => (tags instanceof Object ? Object.keys(tags).length : 0) > max

const articleValidators = (type) => {
  let validation = {}
  if (type === 'blog') {
    validation = {
      title: {
        required,
        maxLength: maxLength(50)
      },
      featuredImage: { required },
      category: { required }
    }
  } else {
    validation = {
      title: {
        required,
        maxLength: maxLength(50)
      },
      thingsToDo: { required },
      featuredImage: { required },
      priorities: { required, selected },
      lifeSkills: {
        min: (value, vm) => {
          if (vm.focus === Quality) {
            return true
          }
          return Boolean(value && Object.keys(value).length)
        },
        max: (value, vm) => {
          return Boolean(vm.focus === Quality ? maxTagsSelected(value, 0) : maxTagsSelected(value, 1)) === false
        }
      },
      qualities: {
        min: (value, vm) => {
          if (vm.focus !== Quality) {
            return true
          }
          return Boolean(value && Object.keys(value).length)
        },
        max: (value, vm) => {
          return Boolean(vm.focus === Quality ? maxTagsSelected(value, 1) : maxTagsSelected(value, 3)) === false
        }
      }
    }
  }
  return validation
}

const maxPlainTextLength = (introTextEditorRef, maxLength) =>
  helpers.withParams(
    { type: 'maxPlainTextLength', value: maxLength },
    function () {
      let componentNotReady = !this.$refs || !this.$refs[introTextEditorRef]
      if (componentNotReady) {
        return false
      }
      let textEditorInstance = this.$refs[introTextEditorRef].quill
      return textEditorInstance.getLength() - 1 <= maxLength
    }
  )

const blocksRequiredIfIntroEmpty = (introTextEditorRef, blocksRef) =>
  helpers.withParams(
    { type: 'blocksRequiredIfIntroEmpty' },
    function () {
      let componentNotReady = !this.$refs || !(this.$refs[blocksRef] && this.$refs[introTextEditorRef])
      if (componentNotReady) {
        return false
      }
      let textEditorInstance = this.$refs[introTextEditorRef].quill
      const introDefined = textEditorInstance.getLength() > 1
      return introDefined || !this.$refs[blocksRef].bodyIsEmpty
    }
  )

const introDifferentToFirstBlock = (introTextEditorRef, blocksRef) =>
  helpers.withParams(
    { type: 'introDifferentToFirstBlock' },
    function (currentIntro) {
      let componentNotReady = !this.$refs || !this.$refs[blocksRef]
      if (componentNotReady) {
        return false
      }
      const bodyEditor = this.$refs[blocksRef]
      const textOfFirstBlock = (
        bodyEditor.$refs &&
        bodyEditor.$refs.textBlocks &&
        bodyEditor.$refs.textBlocks[0] &&
        bodyEditor.$refs.textBlocks[0].content
      )
      if (textOfFirstBlock) {
        return currentIntro !== textOfFirstBlock
      }
      return true
    }
  )

// Function returning additional validators requiring access to UI components
// E.g. in order to calculate length of a formatted text (stored in HTML format), a validator requires access to text-editor component to fetch the actual plain text length (as opposed to length of HTML contents)
const articleUiValidators = (introTextEditorRef, blocksEditorRef, type) => {
  let validators = { ...articleValidators(type) }
  validators.intro = {
    maxLength: maxPlainTextLength(introTextEditorRef, type === 'blog' ? 9999 : 140),
    differentToFirstBlock: introDifferentToFirstBlock(introTextEditorRef, blocksEditorRef)
  }
  validators.blocks = {
    required: blocksRequiredIfIntroEmpty(introTextEditorRef, blocksEditorRef)
  }
  return validators
}

export default articleValidators
export {articleUiValidators}
