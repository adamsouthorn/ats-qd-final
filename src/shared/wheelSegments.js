export default {
  'seg1': 'Health and Wellbeing',
  'seg2': 'Home and Environment',
  'seg3': 'Relationships and Family',
  'seg4': 'Community and Leisure',
  'seg5': 'Work and Purpose',
  'seg6': 'Security and Finances'
}
