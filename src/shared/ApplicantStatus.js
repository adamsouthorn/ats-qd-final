const NotSubmitted = 'not_submitted'
const InProgress = 'in_progress'
const SubmittedForReview = 'submitted_for_review'
const Rejected = 'rejected'
const Accepted = 'accepted'

const statusMap = {
  NotSubmitted,
  InProgress,
  SubmittedForReview,
  Rejected,
  Accepted
}

export const statusTitles = {
  [NotSubmitted]: 'Not submitted',
  [InProgress]: 'In progress',
  [SubmittedForReview]: 'Submitted for review',
  [Rejected]: 'Rejected',
  [Accepted]: 'Accepted'
}

export const statusList = Object.values(statusMap)
export default statusMap
