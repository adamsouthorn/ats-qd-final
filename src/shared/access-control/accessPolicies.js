import Permission from './Permission'
import { Admin, Moderator, ContentProvider, Applicant, BlogProvider, Marketing } from './UserRole'

export default {
  [Permission.ListUsers]: [Admin],
  [Permission.UpdateUsers]: [Admin],
  [Permission.UpgradeUsers]: [Admin, Moderator],
  [Permission.ListAuthors]: [Admin, Moderator],
  [Permission.ListArticles]: [Admin, Moderator],
  [Permission.ListBlogs]: [Admin, Moderator, BlogProvider],
  [Permission.ListArticlesInReview]: [Admin, Moderator],
  [Permission.ListBlogsInReview]: [Admin, Moderator],
  [Permission.CreateArticles]: [Admin, Moderator, ContentProvider],
  [Permission.CreateBlogs]: [Admin, Moderator, BlogProvider],
  [Permission.UpdateOwnUser]: [Admin, Moderator, ContentProvider, BlogProvider, Applicant],
  [Permission.UpdateArticles]: [Admin, Moderator],
  [Permission.UpdateBlogs]: [Admin, Moderator, BlogProvider],
  [Permission.PublishArticles]: [Admin, Moderator],
  [Permission.PublishBlogs]: [Admin, Moderator],
  [Permission.ViewContentStats]: [Admin, Moderator],
  [Permission.ViewToolUsageStats]: [Admin],
  [Permission.AccessContent]: [Admin, Moderator, ContentProvider, BlogProvider],
  [Permission.AccessArticles]: [Admin, Moderator, ContentProvider],
  [Permission.AccessBlog]: [Admin, Moderator, BlogProvider],
  [Permission.AccessMarketing]: [Admin, Marketing],
  [Permission.AccessPortal]: [Admin, Moderator, ContentProvider, BlogProvider, Applicant],
  [Permission.ViewReferralStats]: [Admin],
  [Permission.ManageLifeSkills]: [Admin, Moderator]
}
