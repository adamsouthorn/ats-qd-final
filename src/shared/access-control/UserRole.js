export const Admin = 'admin'
export const ContentProvider = 'contentProvider'
export const Moderator = 'moderator'
export const BlogProvider = 'blogProvider'
export const Applicant = 'applicant'
export const Marketing = 'marketing'

const roleMap = {
  Admin,
  ContentProvider,
  Moderator,
  BlogProvider,
  Applicant,
  Marketing
}

export const roleList = Object.values(roleMap)

export const roleNameMap = {
  [Admin]: 'Admin',
  [ContentProvider]: 'Content Provider',
  [Moderator]: 'Moderator',
  [BlogProvider]: 'Blog Provider',
  [Applicant]: 'Applicant',
  [Marketing]: 'Marketing'
}

export default roleMap
