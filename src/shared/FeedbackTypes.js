const Publish = 'publish'
const Reject = 'reject'

const feedbackTypes = {
  Publish,
  Reject
}

export default feedbackTypes
