const Video = 'video'
const Article = 'article'

const typeUI = (name, icon) => ({ name, icon })

const typeMap = {
  Video,
  Article
}

export const typeList = Object.values(typeMap)

export const typeDisplayProperties = {
  [Video]: typeUI('Video', 'play_circle_outline'),
  [Article]: typeUI('Article', 'description')
}

export default typeMap
