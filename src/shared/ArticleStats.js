import { getVideoJson } from 'plugins/axios/wistia'
import { firebase } from 'plugins/firebase'

const calculateTimeToReadTextInMs = text => {
  const WPM = 250
  const WORDS_PER_SECOND = WPM / 60
  const WORD_MATCH_REGEX = /(['"]?\w+[.,'"]?)+/g
  const wordsArray = text.match(WORD_MATCH_REGEX) || []
  const numberOfWords = wordsArray.length
  return (numberOfWords / WORDS_PER_SECOND) * 1000
}

const extractReadableTextFromHTML = html => {
  const parser = new DOMParser()
  const doc = parser.parseFromString(html, 'text/html')
  const [ body ] = doc.getElementsByTagName('body')
  return body instanceof Element ? body.innerText : ''
}

const getReadTimeFromBodies = content => {
  const blockExtractedText = extractReadableTextFromHTML(content)
  const blockTime = calculateTimeToReadTextInMs(blockExtractedText)
  return blockTime
}

// Set article read time
export const updateArticleReadTime = async articleId => {
  let readTime = 0
  const blocksSnapshot = await firebase.content
    .collection(`bodies/${articleId}/blocks`)
    .get()
  blocksSnapshot.forEach(block => {
    const content = block.get('content')
    if (content) {
      readTime += getReadTimeFromBodies(content)
    }
  })
  return readTime
}

// Set article watch time
export const updateArticleWatchTime = async article => {
  let watchTime = 0
  if (
    article.featuredVideo &&
    article.featuredVideo.mediaId
  ) {
    const videoMeta = await getVideoJson(article.featuredVideo.mediaId)
    if (
      videoMeta &&
      videoMeta.data &&
      videoMeta.data.media &&
      videoMeta.data.media.duration
    ) {
      watchTime += videoMeta.data.media.duration * 1000
    }
  }
  return watchTime
}
