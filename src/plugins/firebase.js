import 'firebase/firestore'

import collection from './firebase/collection'
import auth from './firebase/auth'
import content from './firebase/content'
import users from './firebase/users'
import accessControl from './firebase/access-control'
import email from './firebase/email'
import tools from './firebase/tools'
import imageObject from './firebase/imageObject'
import authorApplicants from './firebase/authorApplicants'
import editor from './firebase/editor'
import logs from './firebase/logs'
import payments from './firebase/payments'

const firebase = {
  collection,
  auth,
  users,
  content,
  email,
  tools,
  imageObject,
  authorApplicants,
  editor,
  logs,
  payments
}

export default ({ Vue }) => {
  Vue.prototype.$firebase = firebase
  Vue.prototype.$accessControl = accessControl
}

export { firebase }
