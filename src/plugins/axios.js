import * as wistia from './axios/wistia'
import * as utils from './axios/utils'

const axiosPlugin = {
  wistia,
  utils
}

export default ({ Vue }) => {
  Vue.prototype.$axios = axiosPlugin
}

export { axiosPlugin as axios }
