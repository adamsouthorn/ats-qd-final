import axios from 'axios'
import config from '../../config'

const uploadInstance = axios.create({
  baseURL: config.wistia.uploadUrl
})

const videoJsonInstance = axios.create({
  baseURL: config.wistia.videoJSONUrl
})

const getVideoJson = async function getVideoJsonp (videoHashId) {
  const videoJson = await videoJsonInstance.get(`${videoHashId}.json`,
    {
      crossDomain: true
    })
  return videoJson
}

const apiInstance = axios.create({
  baseURL: config.wistia.apiUrl,
  params: {
    api_password: `${config.wistia.key}`
  }
})

const uploadVideo = async function uploadVideo (videoFile) {
  let formData = new FormData()
  formData.append('file', videoFile)
  const videoUploaded = await uploadInstance.post('', formData,
    {
      crossDomain: true,
      headers: { 'Content-Type': 'multipart/form-data' },
      params: {
        'api_password': `${config.wistia.key}`,
        'project_id': 'nsxxa102wo'
      }
    })
  return videoUploaded
}

const deleteVideo = async function deletVideo (mediaId) {
  const videoDeleted = await apiInstance.delete(`/medias/${mediaId}.json`,
    {
      crossDomain: true
    })
  return videoDeleted
}

export {
  uploadVideo,
  deleteVideo,
  getVideoJson
}
