import axios from 'axios'

const downloadFile = function downloadFile (url) {
  return axios.get(url, {
    responseType: 'blob'
  }).then(response => {
    return new Blob([response.data])
  }).catch(response => {
    return null
  })
}

export {
  downloadFile
}
