import Vue from 'vue'

import * as SentryBrowser from '@sentry/browser'
import { Vue as SentryVue } from '@sentry/integrations'

import configuration from 'config'

function DisableForLocalBuilds (target, name, descriptor) {
  if (process.env.DEV) {
    const CONSOLE_LOG_STYLING = 'background: red; color: white;'
    descriptor.value = (...args) => console.log(
      `%c ${target.name}.${name} `,
      CONSOLE_LOG_STYLING,
      '(', args, ')'
    )
  }
}

export default class Sentry {
  @DisableForLocalBuilds
  static init () {
    const sentryVueIntegration = new SentryVue({ Vue, attachProps: true })

    const environment = configuration.sentry.env
    const release = process.env.COMMIT_HASH || 'Unknown'

    SentryBrowser.init({
      dsn: configuration.sentry.dsn,
      integrations: [sentryVueIntegration],
      environment,
      release
    })
  }
}
