import firebase from 'firebase'
import { ARTICLES_COLLECTION, BLOGS_COLLECTION } from './index'

export default (id, priorities, isBlog) => {
  const ref = firebase.firestore().collection(
    isBlog ? BLOGS_COLLECTION : ARTICLES_COLLECTION
  ).doc(id)

  // Set Firestore Priorities
  ref.set({
    priorities
  }, {
    merge: true
  })
}
