# How Priorities / Segments are Stored
Since segments are not directly correlated with their corresponding segments, I have decided to add a new field to content articles containing the priorities.

The priorities are split into 4 sections:
- My Self
- My Work
- My Relationships
- My Environments

Each of these sections is referred to as an area, and each area is given its own UID (called auid - area uid).

Within each are 4 subsections, where each denotes a priority. For each of these priorities, there is a UID (called puid - priority uid) starting at 1 within each area.

This approach means that if new areas are added, a new AUID should be added, which will follow from the last, and for any new priorities within a section, a new PUID following on from the last in that **area** should be created. This is to avoid the case where a priority is added to the first area, and must be delegated a PUID that isnot consecutive to the rest of those priorities in that segment
