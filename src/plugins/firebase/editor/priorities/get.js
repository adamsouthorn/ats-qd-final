import firebase from 'firebase'
import { ARTICLES_COLLECTION, BLOGS_COLLECTION } from './index'

export default (id, callback, isBlog) => {
  const ref = firebase.firestore().collection(
    isBlog ? BLOGS_COLLECTION : ARTICLES_COLLECTION
  ).doc(id)

  // Get any updates to object
  ref.onSnapshot(
    doc => callback(
      (doc.data() && doc.data().priorities) || {}
    )
  )
}
