import get from './get'
import set from './set'

export const ARTICLES_COLLECTION = 'content/v1/articles'
export const BLOGS_COLLECTION = 'content/v1/blogs'

export default {
  get,
  set
}
