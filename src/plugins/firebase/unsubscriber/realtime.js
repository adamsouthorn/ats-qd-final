import { Tracker, Manager } from './tracker'

export class RealtimeListenerManager {
  constructor () {
    this._cb = null
    this._ref = null
  }

  get _subscribed () {
    return this._cb && this._ref
  }

  _set (ref = null, callback = null) {
    this._cb = callback
    this._ref = ref
  }

  subscribe (ref, callback) {
    if (!this._subscribed) {
      this._set(ref, callback)
      this._ref.on('value', this._cb)
    }
  }

  unsubscribe () {
    if (this._subscribed) {
      this._ref.off('value', this._cb)
      this._set()
    }
  }
}

export class RealtimeRefsManager extends Manager {
  _model () {
    return {
      manager: new RealtimeListenerManager(),
      tracker: new Tracker()
    }
  }
}
