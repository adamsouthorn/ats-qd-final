export class Tracker {
  constructor () {
    this._listeners = []
  }

  listen (uid) {
    const index = this._listeners.indexOf(uid)
    if (index < 0) {
      this._listeners.push(uid)
    }
  }

  unlisten (uid) {
    const index = this._listeners.indexOf(uid)
    if (index > -1) {
      this._listeners.splice(index, 1)
    }
  }

  get shouldDestroy () {
    return !this._listeners.length
  }
}

export class Manager {
  constructor () {
    this._refs = {}
  }

  _initRef (uid) {
    this._refs[uid] = this._model()
  }

  _ref (uid) {
    return this._refs[uid]
  }

  _manager (uid) {
    const ref = this._ref(uid)
    return ref && ref.manager
  }

  _tracker (uid) {
    const ref = this._ref(uid)
    return ref && ref.tracker
  }

  add ({ uid, componentId }, { node, callback }) {
    if (!this._ref(uid)) {
      this._initRef(uid)
    }

    this._tracker(uid).listen(componentId)
    this._manager(uid).subscribe(node, callback)
  }

  remove ({ uid, componentId }) {
    if (this._ref(uid)) {
      this._tracker(uid).unlisten(componentId)
      if (this._tracker(uid).shouldDestroy) {
        this._manager(uid).unsubscribe()
      }
    }
  }
}
