import { Tracker, Manager } from './tracker'

export class FirestoreListenerManager {
  constructor () {
    this._unsubscriber = null
  }

  _subscribed () {
    return this._unsubscriber
  }

  _set ({ unsubscriber = null }) {
    this._unsubscriber = unsubscriber
  }

  subscribe ({ doc, callback }) {
    if (!this._subscribed) {
      const unsubscriber = doc.onSnapshot(callback)
      this._set({ unsubscriber })
    }
  }

  unsubscribe () {
    if (this._subscribed) {
      this._unsubscriber()
      this._set({})
    }
  }
}

export class FirestoreDocsManager extends Manager {
  _model () {
    return {
      manager: new FirestoreListenerManager(),
      tracker: new Tracker()
    }
  }
}
