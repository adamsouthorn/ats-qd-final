import firebase from 'firebase'

const docs = snapshot => snapshot.docs.map(doc => doc.data())
const actionLog = action => firebase.firestore().collection(
  'logs'
).where(
  'action',
  '==',
  action
).orderBy(
  'timestamp',
  'desc'
).limit(
  100
)

export default (action, callback) => callback instanceof Function
  ? actionLog(action).onSnapshot(
    snapshot => callback(docs(snapshot))
  )
  : actionLog(action).get().then(docs)
