import firebase from 'firebase/app'
import 'firebase/firestore'

const firestoreInstance = firebase.firestore()

const ref = (collectionName) => {
  return firestoreInstance.collection(collectionName)
}

const getFirst = async (collectionReference) => {
  let collectionSnapshot = await collectionReference.limit(1).get()
  return !collectionSnapshot.empty ? collectionSnapshot.docs[0].data() : null
}

export default {
  ref,
  getFirst
}
