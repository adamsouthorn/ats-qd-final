
import firebase from 'firebase/app'
import 'firebase/database'

import config from './../../config'

export const generateBulkVouchers = (params) => firebase.functions().httpsCallable(
  config.cloudFunctions.onGenerateBulkVouchers
)(params)

export default {
  generateBulkVouchers
}
