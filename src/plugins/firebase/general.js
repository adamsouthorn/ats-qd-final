import firebase from 'firebase'

/**
 *
 * @param {String} referenceName
 * @param {String} itemName
 */
export const getDataValue = async (referenceName, itemName) => {
  const reference = await firebase.database().ref(referenceName).child(itemName)
  const snapshot = await reference.once('value')

  return snapshot.val()
}
