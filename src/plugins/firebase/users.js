import firebase from 'firebase/app'
import 'firebase/database'

import config from './../../config'

const authorRootRef = firebase.database().ref(`${config.api.authors}`)
const usersRootRef = firebase.database().ref(`${config.api.users}`)
import AuthorStatus from 'shared/ApplicantStatus'

const AUTHOR_PROFILE_SAVED_KEY = 'authorSavedProfile'

const getUserRef = (userId) => {
  return usersRootRef.child(`${userId}`)
}

const getUserProfileRef = (userId) => {
  return getUserRef(userId).child('profile')
}

const userAuthorProfileCompleted = async (userId) => {
  let userAuthorRef = await getUserAuthorRef(userId)
  if (!userAuthorRef) {
    return
  }
  let completedRef = userAuthorRef.ref.child('profile').child(AUTHOR_PROFILE_SAVED_KEY)
  return (await completedRef.once('value')).val()
}

const getUserAuthorIdRef = (userId) => {
  return getUserProfileRef(userId).child('defaultAuthor')
}

const getUserAuthorRef = async (userId) => {
  let authorQuerySnapshot = await getUserAuthorIdRef(userId).once('value')
  if (authorQuerySnapshot.exists()) {
    let authorId = authorQuerySnapshot.val()
    return authorRootRef.child(authorId)
  }
  return null
}

const createAuthorProfileForUser = async (userId, initialProfileData, status = AuthorStatus.Accepted) => {
  let newAuthorRef = authorRootRef.push()
  let authorData = {
    users: { [userId]: true },
    defaultUser: userId,
    status
  }
  if (initialProfileData) {
    const { email, ...profileData } = initialProfileData
    authorData.profile = profileData
    authorData.email = email
  }
  await newAuthorRef.set(authorData)
  await getUserProfileRef(userId).update({
    defaultAuthor: newAuthorRef.key
  })

  return newAuthorRef
}

const getOrCreateAuthorProfileForUser = async (userId, initialProfileData) => {
  let foundAuthorProfile = await getUserAuthorRef(userId)
  if (foundAuthorProfile) {
    return foundAuthorProfile.ref
  }
  return createAuthorProfileForUser(userId, initialProfileData)
}

import imageObject from './imageObject'
import { axios } from 'plugins/axios'

const importAuthorAvatarFromUrl = async (authorRef, existingPhotoUrl) => {
  let avatarRef = authorRef.child('profile/avatar')
  let cpAlreadyHasAvatar = (await avatarRef.once('value')).val() !== null

  if (cpAlreadyHasAvatar) {
    return
  }
  let fileBlob = await axios.utils.downloadFile(existingPhotoUrl)
  let fileNotFound = !!fileBlob
  if (!fileNotFound) {
    return
  }

  await imageObject({
    firebaseDir: avatarRef.path,
    storageDir: `authors/${authorRef.key}/avatar`
  }).uploadNewImage(fileBlob, 'avatar')
}

const getUserByEmail = function getUserByEmail (email) {
  const getUserByEmailCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.findUserByEmail
  )
  return getUserByEmailCloudFunction({
    email
  })
}

const getUsersInBulk = function getUsersInBulk (pageToken) {
  const getUsersBulkCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.getUsersInBulk
  )
  return getUsersBulkCloudFunction({
    pageToken
  })
}

const getModerators = (options, pageToken) => {
  const getModeratorsCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.getModerators
  )
  return getModeratorsCloudFunction({
    pageToken,
    ...options
  })
}

const getAllModerators = async (options, callback) => {
  let batch = null
  while (!batch || batch.length > 0) {
    batch = (await getModerators(options, batch && batch[batch.length - 1] && batch[batch.length - 1].displayName)).data
    if (!batch || batch.length > 0) callback(batch)
  }
}

export default {
  getOrCreateAuthorProfileForUser,
  createAuthorProfileForUser,
  importAuthorAvatarFromUrl,
  getUserRef,
  getUserAuthorRef,
  authorList: authorRootRef,
  userList: usersRootRef,
  getUserByEmail,
  getUsersInBulk,
  userAuthorProfileCompleted,
  getAllModerators,
  AUTHOR_PROFILE_SAVED_KEY
}
