
import firebase from 'firebase/app'
import 'firebase/firebase-functions'
import 'firebase/firestore'

export const init = function () {
  const app = firebase.initializeApp(process.env.firebaseConfig)
  const useFunctionEmulator = process.env.USE_CLOUD_FUNCTIONS_EMULATOR
  if (useFunctionEmulator) {
    app.functions().useFunctionsEmulator(
      `http://localhost:${process.env.FUNCTIONS_EMULATOR_PORT || 5000}`
    )
  }
}
