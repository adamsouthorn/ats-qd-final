import { default as FirestoreCollection } from './collection'
import { config } from './../../config'

import firebase from 'firebase/app'
import 'firebase/storage'

import * as listen from 'plugins/firebase/content/listen'
import * as get from 'plugins/firebase/content/get'

const collection = (collectionName) => {
  return FirestoreCollection.ref(
    `content/${config.firestore.version}/${collectionName}`
  )
}

const fileRef = (filePath) => {
  return firebase.app().storage().ref(filePath)
}
const imageObjectFiles = (imageObject) => {
  return [
    imageObject.originalFilePath,
    imageObject.croppedFiles && imageObject.croppedFiles[0].filePath
  ].filter(Boolean)
}
const updateImageObjectsMetadata = (imageObjects, newMetadata) => {
  let updateMetadataPromises = imageObjects
    .map(imageObjectFiles)
    .reduce((a, b) => a.concat(b), [])
    .map(
      filePath => fileRef(filePath).updateMetadata({
        customMetadata: newMetadata
      })
    )
  return Promise.all(updateMetadataPromises)
}

const getUserContentArticles = (userId) => {
  return collection('articles').where('owner', '==', userId)
}

const doesUserHaveAnyArticles = async (userId) => {
  return new Promise((resolve) => {
    getUserContentArticles(userId).limit(1).get().then((articles) => {
      resolve(!!articles.size)
    })
  })
}

export default {
  collection,
  articles (type) {
    let articlesCollection = collection(type)
    const get = articleId => {
      let document = articlesCollection.doc(articleId)
      const updateImageMetadata = async (newMetadata) => {
        let articleBlocksCollection = collection(`bodies/${articleId}/blocks`)
        let articleData = (await document.get()).data()
        let articleBlocks = (await articleBlocksCollection.get()).docs.map(doc => doc.data())
        let imageObjects = articleBlocks.filter(block => block.type === 'image').map(block => block.imageData)
        if (articleData.featuredImage) {
          imageObjects.push(articleData.featuredImage)
        }
        let articleDoesNotContainImages = imageObjects.length === 0
        if (articleDoesNotContainImages) {
          return Promise.resolve()
        }
        return updateImageObjectsMetadata(imageObjects, newMetadata)
      }
      return {
        document,
        updateImageMetadata
      }
    }

    return {
      get,
      all: articlesCollection
    }
  },
  listen,
  get,
  doesUserHaveAnyArticles
}
