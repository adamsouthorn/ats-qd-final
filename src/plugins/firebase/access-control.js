import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/functions'
import Vue from 'vue'

import { default as auth } from './auth'
import { config } from './../../config'

let tokenRefreshTimeUpdateCallback = null
let tokenRefreshTimeRef = null
let pluginConfig = {
  onUserRolesChanged: null,
  accessPoliciesMap: null
}

// Consider using Vuex instead
let currentUserState = new Vue({
  data: {
    roles: null
  },
  computed: {
    isContentProvider () {
      return !!this.roles.contentProvider
    },
    isContentAdmin () {
      return !!this.roles.admin || !!this.roles.moderator
    },
    isAdmin () {
      return !!this.roles.admin
    },
    isApplicant () {
      return !!this.roles.applicant
    }
  },
  methods: {
    setUserRoles (roles) {
      this.roles = roles
    }
  }
})

const getCurrentUserRoles = async function getCurrentUserRoles () {
  let user = await auth.getCurrentUser()
  let tokenResult = await user.getIdTokenResult(false)
  currentUserState.setUserRoles(tokenResult.claims)
  return tokenResult.claims
}

const refreshTokenAndUpdateRolesState = async function refreshTokenAndUpdateRolesState (user) {
  let tokenResult = await user.getIdTokenResult(true)
  currentUserState.setUserRoles(tokenResult.claims)
  if (typeof pluginConfig.onUserRolesChanged === 'function') {
    pluginConfig.onUserRolesChanged(tokenResult.claims)
  }
}

const subscribeToTokenRefreshUpdates = function subscribeToTokenRefreshUpdates () {
  firebase.app().auth().onAuthStateChanged(async function (user) {
    if (tokenRefreshTimeUpdateCallback) {
      tokenRefreshTimeRef.off('value', tokenRefreshTimeUpdateCallback)
    }
    if (user) {
      tokenRefreshTimeRef = firebase.database().ref(`${config.api.metadata}/users/${user.uid}/tokenRefreshTime`)
      let initialCallbackExecuted = false
      tokenRefreshTimeUpdateCallback = async (snapshot) => {
        if (!initialCallbackExecuted) {
          initialCallbackExecuted = true
          return
        }
        if (!user) {
          return
        }
        await refreshTokenAndUpdateRolesState(user)
      }
      tokenRefreshTimeRef.on('value', tokenRefreshTimeUpdateCallback)
    } else {
      currentUserState.setUserRoles(null)
    }
  })
}

export const userAllowedTo = function userAllowedTo (permission) {
  let userRoles = currentUserState.roles
  if (!userRoles) {
    return false
  }
  let allowedRoles = pluginConfig.accessPoliciesMap[permission]
  let roleFound = allowedRoles.find(role => userRoles[role] === true)
  return !!roleFound
}

const fetchRolesAndCheckIfUserAllowedTo = async function fetchRolesAndCheckIfUserAllowedTo (permission) {
  await getCurrentUserRoles()
  return userAllowedTo(permission)
}

const fetchRolesAndCheckIfUserHasARole = async function fetchRolesAndCheckIfUserHasARole (role) {
  await getCurrentUserRoles()
  return currentUserState.roles && !!currentUserState.roles[role]
}

const initialize = function initialize ({ onUserRolesChanged, accessPoliciesMap }) {
  pluginConfig.onUserRolesChanged = onUserRolesChanged
  pluginConfig.accessPoliciesMap = accessPoliciesMap
  subscribeToTokenRefreshUpdates()
}

export default {
  initialize,
  userAllowedTo,
  fetchRolesAndCheckIfUserAllowedTo,
  fetchRolesAndCheckIfUserHasARole,
  refreshTokenAndUpdateRolesState,
  user: currentUserState
}
