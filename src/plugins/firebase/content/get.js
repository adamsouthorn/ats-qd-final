import firebase from 'firebase'

export const collection = (type) => firebase.firestore().collection(
  type === 'blogs' ? 'content/v1/blogs' : 'content/v1/articles'
)

export const newRef = () => collection().doc()
export const ref = (id) => collection().doc(id)

export const article = (id) => ref(id).get()
export const articlesForSkill = (skillId) => collection().where(`lifeSkills.${skillId}`, '==', true)
