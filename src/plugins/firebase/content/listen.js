import firebase from 'firebase'
import { config } from 'src/config'
import { userAllowedTo } from 'plugins/firebase/access-control'
import Permissions from 'shared/access-control/Permission'

export const all = async (callback) => {
  const collection = firebase.firestore().collection(
    `content/${config.firestore.version}/articles`
  )

  const user = firebase.auth().currentUser

  console.log(user)

  const query = userAllowedTo(Permissions.ListArticles)
    ? collection
    : collection.where(
      'owner',
      '==',
      user.uid
    )

  return callback instanceof Function
    ? query.onSnapshot(snapshot => {
      const data = snapshot.docs.map(doc => ({
        ...doc.data(),
        uid: doc.id
      }))
      callback(data)
    })
    : query.get()
}
