import firebase from 'firebase/app'
import 'firebase/database'

import { config } from 'src/config'
import { RealtimeRefsManager } from 'plugins/firebase/unsubscriber'

/**
 * getUsefulRef() - Gets useful node in the realtime database
 * getContentUsefulRef() - Gets likes node for particular article in realtime database
 */
const getUsefulRef = () => firebase.database().ref(`${config.api.useful}`)
export const getContentUsefulRef = articleUID => getUsefulRef().child(articleUID)
export const getContentUsefulMeta = articleUID => getContentUsefulRef(articleUID).child('meta')

const LikeListenersManager = new RealtimeRefsManager()

export const unlistenToArticleUsefulness = ({ uid, componentId }) => {
  LikeListenersManager.remove({ uid, componentId })
}

export const listenToArticleUsefulness = ({ uid, componentId }, callback) => {
  const ref = getContentUsefulMeta(uid)

  LikeListenersManager.add({
    uid,
    componentId
  }, {
    node: ref,
    callback: snapshot => {
      const {
        usefulScore: score = 0
      } = snapshot.val() || {}

      if (!score) {
        callback(null)
      } else {
        callback(score)
      }
    }
  })
}
