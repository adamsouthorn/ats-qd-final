import firebase from 'firebase'

const searchFunction = data => {
  const findArticlesFn = firebase.functions().httpsCallable(
    'findArticles'
  )
  return findArticlesFn(data)
}

const constructFilters = (term, {focus, status, priorities, lifeSkills, qualities, dateFrom, dateTo}) => {
  let filtersList = []
  if (status) {
    filtersList.push(
      typeof status === 'string' ? { term: { status } } : { terms: { status } }
    )
  }
  if (focus) {
    filtersList.push({
      term: {
        focus
      }
    })
  }
  if (priorities) {
    filtersList.push({
      term: {
        [`priorities.${priorities}`]: true
      }
    })
  }
  if (lifeSkills) {
    filtersList.push({
      terms: {
        lifeSkills: [lifeSkills]
      }
    })
  }
  if (qualities) {
    filtersList.push({
      terms: {
        qualities: [qualities]
      }
    })
  }
  if (dateTo && dateFrom && dateTo > 0 && dateFrom > 0) {
    filtersList.push({
      range: {
        createdTime: {
          gte: dateFrom,
          lte: dateTo
        }
      }
    })
  }
  if (term === 'Video' || term === 'Article') {
    filtersList.push({
      term: {
        video: term === 'Video'
      }
    })
  }
  return {
    filter: filtersList
  }
}

const getSortParams = ({descending, sortBy}) => {
  if (sortBy) {
    const sortByField = sortBy === 'updatedTime' ? 'updatedTimeTimestamp' : sortBy

    return {
      sort: {
        [sortByField]: descending ? 'desc' : 'asc'
      }
    }
  }
}

const constructTextQuery = (term) => {
  if (term === 'Video' || term === 'Article') {
    return
  }
  if (term) {
    const fieldNames = ['title.search', 'authorName.search', 'updatedTime']
    const textFilters = fieldNames.map(key => ({
      match_phrase_prefix: {
        [key]: term
      }
    }))
    return {
      must: {
        bool: {
          should: textFilters
        }
      }
    }
  }
}
export const search = ({from, size, filters, term, sortBy, descending}) => {
  let queryObj = {
    query: {
      bool: {
        ...constructFilters(term, filters),
        ...constructTextQuery(term)
      }
    },
    ...getSortParams({sortBy, descending}),
    from,
    size
  }
  return searchFunction(queryObj)
}
