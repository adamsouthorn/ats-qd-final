import firebase from 'firebase/app'
import 'firebase/database'

import config from './../../config'
import AuthorStatus from 'shared/ApplicantStatus'
import Roles from 'shared/access-control/UserRole'
import auth from './auth'
import users from './users'
import emailFunctions from './email'
import accessControl from './access-control'
import { intercom } from 'plugins/intercom'
import * as Events from 'plugins/intercom/events'

const authorsRootRef = firebase.database().ref(`${config.api.authors}`)
const usersRootRef = firebase.database().ref(`${config.api.users}`)

const getUserApplicantIdRef = (userId) => {
  return usersRootRef
    .child(userId)
    .child('/profile/defaultAuthor')
}

const addNewApplicant = async (userId, initialProfileData) => {
  let newAuthorRef = await users.createAuthorProfileForUser(
    userId,
    initialProfileData,
    AuthorStatus.NotSubmitted
  )
  logSignInTime(userId)
  return newAuthorRef
}

const getApplicantForUser = async userId => {
  let applicantRefId = await getUserApplicantIdRef(userId)
  let applicantId = (await applicantRefId.once('value')).val()
  if (!applicantId) {
    return null
  }
  return authorsRootRef.child(applicantId)
}

const completeApplicantProfile = async userId => {
  let applicantRef = await getApplicantForUser(userId)
  await auth.setCustomUserClaims(
    userId,
    { [Roles.ContentProvider]: true },
    [Roles.Applicant]
  )
  await applicantRef.update({
    status: AuthorStatus.InProgress
  })
  intercom.logEvent(Events.ProfileCompleted)
}

const rejectApplicant = async (userId, email, name) => {
  let applicantRef = await getApplicantForUser(userId)
  await applicantRef.update({
    status: AuthorStatus.Rejected
  })
  await auth.setCustomUserClaims(
    userId,
    {},
    [Roles.ContentProvider, Roles.Applicant]
  )
  if (email) {
    await emailFunctions.sendAuthorRejectEmail(email, name)
  }
}
const markApplicantAsApproved = async userId => {
  let applicantRef = await getApplicantForUser(userId)
  await applicantRef.update({
    status: AuthorStatus.Accepted
  })
}
const promoteApplicantToAnAuthor = async (userId, email, name) => {
  await markApplicantAsApproved(userId)
  await emailFunctions.sendAuthorApproveEmail(email, name)
}

const registerApplicantUser = async (name, email, password, user, marketing) => {
  if (!user) {
    user = await auth.registerUserAndSignIn(
      name,
      email,
      password
    )
    // Reassign head object based on user object
    user = user.user
  }
  await auth.setCustomUserClaims(user.uid, {
    [Roles.Applicant]: true
  })
  await accessControl.refreshTokenAndUpdateRolesState(user)
  await addNewApplicant(
    user.uid,
    {
      displayName: name,
      email
    }
  )
  await usersRootRef.child(user.uid).child('/privacySettings/optOutOfEmails').set(!marketing)
  return user
}

const logSignInTime = async (userId) => {
  let applicantRef = await getApplicantForUser(userId)
  if (!applicantRef) {
    return
  }
  await applicantRef.update({
    lastSignInTime: firebase.database.ServerValue.TIMESTAMP
  })
}

export default {
  registerApplicantUser,
  getApplicantForUser,
  completeApplicantProfile,
  markApplicantAsApproved,
  rejectApplicant,
  logSignInTime,
  promoteApplicantToAnAuthor,
  all: authorsRootRef.orderByChild('status').startAt(AuthorStatus.InProgress).endAt(AuthorStatus.SubmittedForReview),
  authors: authorsRootRef.orderByChild('status').equalTo(AuthorStatus.Accepted)
}
