import firebase from 'firebase/app'
import 'firebase/database'
const database = firebase.database()

import { QUALITIES_PATH, USER_QUALITIES_PATH } from './index.js'

const getDataFromRef = async (ref) => (await ref.once('value')).val()
const getAllAndSelected = () => Promise.all([ all(), selectedUids() ])

const flatten = q => q.map((q) => ({ ...q.quality, uid: q.uid }))
const randIntTo = (n) => Math.floor(Math.random() * n)

// Filters
const filterSelected = (items) => Object.keys(items).filter(i => items[i].selected)
const filterWithUid = (items, uids) => items.filter(i => uids.includes(i.uid))
const filterWithoutUid = (items, uids) => items.filter(i => !uids.includes(i.uid))

/**
 * Returns UIDS of all selected qualities
 */
const selectedUids = async () => {
  const ref = await database.getFirebaseUserChildRefAsync(USER_QUALITIES_PATH)
  const uids = await getDataFromRef(ref)
  const selected = uids
    ? filterSelected(uids)
    : []
  return selected
}

/**
 * Returns random selection of items from pool of items
 * @param {Number} n - Number of random qualities to get
 * @param {Array<Quality>} qs - Pool to select from
 */
const selectRandN = (n, qs, selected = []) => {
  if (n === 0) return []
  const rnd = randIntTo(n)

  selected.push(qs[rnd])
  qs.splice(rnd, 1)

  if (selected.length < n) return selectRandN(n, qs, selected)
  return selected
}

/**
 * Returns all qualities
 */
export const all = async () => {
  const ref = database.ref(QUALITIES_PATH)
  const q = await getDataFromRef(ref)
  return flatten(q)
}

/**
 * Returns all selected qualities
 */
export const selected = async () => {
  const [qs, selected] = await getAllAndSelected()
  return filterWithUid(qs, selected)
}

/**
 * Returns N random qualities
 * @param {Number} n - Number of random qualities to get
 * @param {Array<Quality>} selection - Currently selected qualities (to omit from random selection)
 */
export const random = async (n, selection) => {
  const [qs, selected] = await getAllAndSelected()

  const uidsToOmit = [ ...selected, ...selection ]
  const pool = filterWithoutUid(qs, uidsToOmit)

  return selectRandN(n, pool)
}
