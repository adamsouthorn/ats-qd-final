import { config } from '../../../../config'

import * as get from './get.js'
import * as set from './set.js'

export const QUALITIES_PATH = `${config.api.tools}/qualities`
export const USER_QUALITIES_PATH = 'tools/qualities/selected'

export default {
  get,
  set
}
