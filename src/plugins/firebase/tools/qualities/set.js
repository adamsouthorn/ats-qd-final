import firebase from 'firebase/app'
import 'firebase/database'
const database = firebase.database()

import { USER_QUALITIES_PATH } from './index.js'

/**
 * Selects or deselects a quality
 * @param {Quality} quality - Quality to select or deselect
 * @param {Boolean} selected - Quality selection status
 */
export const selected = (quality, selected) => new Promise(async (resolve, reject) => {
  const ref = await database.getFirebaseUserChildRefAsync(`${USER_QUALITIES_PATH}/${quality.uid}`)
  const query = selected
    ? ref.set({
      selected,
      timestamp: database.getServerTimestamp()
    })
    : ref.remove()
  query.then(() => {
    resolve()
  }).catch((err) => {
    reject(err)
  })
})

/**
 * Toggle selection of quality
 * @param {Quality} quality - Quality to toggle selection
 */
export const toggle = async (quality) => selected(quality, quality.selected)
