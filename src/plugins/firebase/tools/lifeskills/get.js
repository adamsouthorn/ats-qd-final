import firebase from 'firebase/app'
import 'firebase/database'
import { LIFE_SKILLS_PATH, LIFE_SKILLS_CATEGORIES_PATH } from './index'

const database = firebase.database()

const getDataFromRef = async (ref) => (await ref.once('value')).val()
const flatten = (l, type = 'skill') => l.map((l) => ({ ...l[type], uid: l.uid }))

export const all = async () => {
  const ref = database.ref(LIFE_SKILLS_PATH)
  const l = await getDataFromRef(ref)
  return Object.values(l)
}

export const categories = async () => {
  const ref = database.ref(LIFE_SKILLS_CATEGORIES_PATH)
  const l = await getDataFromRef(ref)
  return flatten(l, 'category')
}
