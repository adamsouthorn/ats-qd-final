const Live = 'live'
const LiveSoon = 'live_soon'
const NotLive = 'not_live'

export default {
  Live,
  LiveSoon,
  NotLive
}
