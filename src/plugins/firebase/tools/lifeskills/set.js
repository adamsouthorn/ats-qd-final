import firebase from 'firebase/app'
import 'firebase/database'
const database = firebase.database()

import { USERS_LIFESKILLS_PATH, LIFE_SKILLS_PATH } from './index.js'

const skillListRef = () => database.ref(LIFE_SKILLS_PATH)
const querySkillById = uid => skillListRef().orderByChild('uid').equalTo(uid).limitToFirst(1)
const getSkillRefById = async uid => {
  const queryRes = await querySkillById(uid).once('value')
  let ref = null
  queryRes.forEach(child => {
    ref = child.ref
    return true
  })
  return ref
}

/**
 * Selects or deselects a life-skill
 * @param {UUID} userId - The user for which to select or deselect
 * @param {LifeSkill} lifeSkill - Life skill to select or deselect
 * @param {Boolean} selected - Life skill selection status
 */
export const selected = (lifeSkill, selected) => new Promise(async (resolve, reject) => {
  const ref = await database.getFirebaseUserChildRefAsync(`${USERS_LIFESKILLS_PATH}/${lifeSkill.uid}`)
  const query = selected
    ? ref.set({
      selected,
      ...lifeSkill,
      timestamp: database.getServerTimestamp()
    })
    : ref.remove()
  query.then(() => {
    resolve()
  }).catch((err) => {
    reject(err)
  })
})

export const setSkillWasLiveFlag = async (skillId) => {
  const ref = await getSkillRefById(skillId)
  return ref.update({
    wasLive: true
  })
}

export const setSkillStatus = async (skillId, status) => {
  const ref = await getSkillRefById(skillId)
  return ref.update({
    status
  })
}

export const setSkillData = async (skillId, data) => {
  const ref = await getSkillRefById(skillId)
  return ref.child('skill').update(data)
}

export const deleteSkill = async (skillId) => {
  const ref = skillListRef()
  const skills = (await ref.once('value')).val()
  const index = skills.findIndex(skill => skill && skill.uid === skillId)
  if (index > -1) {
    skills.splice(index, 1)
    await ref.set(skills)
  } else {
    throw new Error('Skill not found')
  }
}

const getHighestSkillIndex = async () => {
  const skillWithHighestIndex = (await skillListRef().orderByKey().limitToLast(1).once('value')).val()
  return Number(Object.keys(skillWithHighestIndex)[0])
}

const getHighestSkillUID = async () => {
  const allSkills = Object.values((await skillListRef().once('value')).val())
  const allSkillIds = allSkills
    .map(skill => skill.uid)
    .map(skillKey => Number(skillKey.substring(1)))
  return Math.max(...allSkillIds)
}

export const addNewSkill = async (skillStatus, skillData) => {
  const newSkillIndex = await getHighestSkillIndex() + 1
  const newSkillUID = `s${await getHighestSkillUID() + 1}`
  const newSkillObject = {
    uid: newSkillUID,
    status: skillStatus,
    skill: skillData
  }
  await skillListRef().child(newSkillIndex).set(newSkillObject)
  return {
    newSkillIndex,
    newSkillObject
  }
}
