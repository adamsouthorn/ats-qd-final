import { config } from '../../../../config'

import * as get from './get'
import * as set from './set'

export const LIFE_SKILLS_PATH = `${config.api.tools}/life-skills`
export const LIFE_SKILLS_CATEGORIES_PATH = `${config.api.tools}/life-skills-categories`
export const USERS_LIFESKILLS_PATH = `tools/life-skills/selected`

export default {
  get,
  set
}
