import lifeSkills from './lifeskills'
import qualities from './qualities'

export default {
  lifeSkills,
  qualities
}
