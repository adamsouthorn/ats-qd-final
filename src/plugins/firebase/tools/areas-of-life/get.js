import firebase from 'firebase/app'
import 'firebase/database'
const database = firebase.database()

import { AREAS_OF_LIFE_PATH } from './index.js'

const getDataFromRef = async (ref) => (await ref.once('value')).val()

export const all = async () => {
  const ref = database.ref(AREAS_OF_LIFE_PATH)
  const q = await getDataFromRef(ref)
  return q
}
