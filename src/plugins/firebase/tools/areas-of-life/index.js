import { config } from '../../../../config'

import * as get from './get.js'

export const AREAS_OF_LIFE_PATH = `${config.api.tools}/areas-of-life`

export default {
  get
}
