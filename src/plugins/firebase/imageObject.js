import firebase from 'firebase/app'
import 'firebase/functions'
import 'firebase/storage'
import config from './../../config'

function ImageObject ({ firebaseDir, storageDir }) {
  if (!firebaseDir || !storageDir) {
    throw new Error('Firebase or storage path not provided')
  }
  let firebaseRef = firebase.app().database().ref(firebaseDir)

  return {
    getFileRef (filePath) {
      return firebase.app().storage().ref(filePath)
    },
    uploadNewImage (fileBlob, fileName = null) {
      return new Promise((resolve) => {
        let filePath = `${config.storage.content}/${storageDir}/${fileName || fileBlob.name}`
        let fileRef = this.getFileRef(filePath)

        const fileUploaded = (fileSnapshot) => {
          fileSnapshot.ref.getDownloadURL().then(async downloadURL => {
            let imageObject = {
              originalFilePath: filePath,
              fileUrl: downloadURL
            }
            await firebaseRef.update(imageObject)
            resolve()
          })
        }
        fileRef.put(fileBlob).then(fileUploaded)
      })
    }
  }
}

export default ImageObject
