import firebase from 'firebase/app'
import 'firebase/functions'
import config from './../../config'

const adminFirebaseApp = firebase.initializeApp(process.env.firebaseConfig, 'admin')

const onAuthStateChanged = async function onAuthStateChanged () {
  const authStatePromise = new Promise((resolve, reject) => {
    let unsubscribe = firebase.auth().onAuthStateChanged(function (result) {
      resolve(result)
      unsubscribe()
    })
  })
  return authStatePromise
}

const currentUser = function currentUser () {
  return firebase.auth().currentUser
}

const getCurrentUser = async function getCurrentUser () {
  let user = currentUser()
  if (user) {
    return user
  } else {
    user = await onAuthStateChanged()
    if (!user) {
      return null
    }
    return user
  }
}

const updateUserDetails = function updateUserDetails (userId, data) {
  const updateUserDetailsCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.updateUserDetails
  )
  return updateUserDetailsCloudFunction({
    userId,
    data
  })
}

const setCustomUserClaims = function setCustomUserClaims (userId, claimsToAdd, claimsToRemove = []) {
  const setCustomUserClaimsCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.setCustomUserClaims
  )
  return setCustomUserClaimsCloudFunction({
    userId,
    claimsToAdd,
    claimsToRemove
  })
}

const registerUserUsingAuthClass = async function registerUserUsingAuthClass (authClass, displayName, email, password) {
  try {
    const createUserPromise = new Promise((resolve, reject) => {
      authClass.createUserWithEmailAndPassword(email, password)
        .then(async res => {
          await res.user.updateProfile({ displayName })
          resolve(res)
        })
        .catch(err => {
          reject(err)
        })
    })
    const userCredential = await createUserPromise
    return userCredential
  } catch (error) {
    throw error
  }
}

const adminCreateUser = function adminCreateUser (displayName, email, password) {
  return registerUserUsingAuthClass(
    adminFirebaseApp.auth(),
    displayName, email, password
  )
}

const registerUserAndSignIn = function registerUserAndSignIn (displayName, email, password) {
  return registerUserUsingAuthClass(
    firebase.auth(),
    displayName, email, password
  )
}

const getAuthUser = function getUser (userId) {
  const getUserListCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.getUser
  )
  return getUserListCloudFunction({
    userId
  })
}

const getAuthUserList = function getUserList () {
  const getUserListCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.getUserList
  )
  return getUserListCloudFunction()
}

const sendPasswordResetEmail = (email) => {
  return firebase.auth().sendPasswordResetEmail(email, { url: window.location.origin })
}

export default {
  setCustomUserClaims,
  updateUserDetails,
  getAuthUser,
  getAuthUserList,
  currentUser,
  getCurrentUser,
  adminCreateUser,
  registerUserAndSignIn,
  sendPasswordResetEmail
}
