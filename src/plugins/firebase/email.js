import firebase from 'firebase/app'
import 'firebase/functions'
import config from './../../config'

const sendAuthorApproveEmail = function sendAuthorApproveEmail (email, displayName) {
  const sendEmailCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.sendAuthorApproveEmail
  )
  return sendEmailCloudFunction({
    email,
    displayName
  })
}

const sendAuthorRejectEmail = function sendAuthorRejectEmail (email, displayName) {
  const sendEmailCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.sendAuthorRejectEmail
  )
  return sendEmailCloudFunction({
    email,
    displayName
  })
}

const onAdminArticleFeedbackEmail = (data) => {
  const sendEmailCloudFunction = firebase.functions().httpsCallable(
    config.cloudFunctions.onAdminArticleFeedbackEmail
  )

  const { userId, articleTitle, url, type, feedback } = data || {}
  return sendEmailCloudFunction({
    userId: userId,
    articleTitle: articleTitle,
    articleLink: url || window.location.href,
    type,
    feedback
  })
}

export default {
  sendAuthorApproveEmail,
  sendAuthorRejectEmail,
  onAdminArticleFeedbackEmail
}
