import VueSanitize from 'vue-sanitize'

export default ({ Vue }) => {
  const defaultOptions = {
    allowedTags: ['a', 'b', 'i', 'p', 'ul', 'li', 'h1', 'h2', 'h3'],
    allowedAttributes: {
      'a': [ 'href', 'target', 'title' ]
    }
  }
  Vue.use(VueSanitize, defaultOptions)
}
