import VueIntercom from 'vue-intercom'
import Vue from 'vue'
import config from '../../config'
import firebase from 'firebase/app'

Vue.use(VueIntercom, { appId: config.intercom.appId })
let intercomPlugin = Vue.prototype.$intercom
const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms))

const intercomPluginMixin = {
  initCalled: false,
  intercomUserInited: false,
  pluginInitPromise: false,
  uid: null,
  intercomReady () {
    if (intercomPlugin._vm.ready) {
      return true
    }
    if (this.pluginInitPromise) {
      return this.pluginInitPromise
    }
    this.pluginInitPromise = new Promise(resolve => {
      const unwatch = intercomPlugin._vm.$watch('ready', function (newVal, oldVal) {
        if (newVal) {
          unwatch()
          resolve()
        }
      })
    }, {
      immediate: true
    })
    return this.pluginInitPromise
  },
  init () {
    if (this.initCalled) {
      return
    }
    this.initCalled = true
    firebase.app().auth().onAuthStateChanged(user => {
      this.syncUser(user)
    })
  },
  async syncUser (user) {
    if (!user) {
      return
    }
    this.uid = user.uid
    await this.intercomReady()
    if (user.isAnonymous) {
      await this.logout()
      intercomPlugin.boot()
      this.intercomUserInited = false
      return
    }
    if (!this.intercomUserInited) {
      await this.logout()
    }
    const updateFn = this.intercomUserInited ? intercomPlugin.update : intercomPlugin.boot
    updateFn(this.intercomUserAttrs(user))
    this.intercomUserInited = true
  },
  async logEvent (eventName, dataObject) {
    await this.intercomReady()
    intercomPlugin.trackEvent(eventName, dataObject)
    this.notifyOfUpdates()
  },
  intercomUserAttrs (user) {
    const portalVersion = process.env.VERSION_NUMBER
    return {
      email: user.email,
      user_id: user.uid,
      name: user.displayName,
      portal_version: portalVersion
    }
  },
  async notifyOfPageChange () {
    await this.intercomReady()
    intercomPlugin.update()
  },
  async notifyOfUpdates () {
    intercomPlugin.update()
    await delay(1000)
    intercomPlugin.update()
    await delay(2000)
    intercomPlugin.update()
    await delay(5000)
    intercomPlugin.update()
  },
  async updateAttributes (data) {
    await this.intercomReady()
    data.user_id = this.uid
    intercomPlugin.update(data)
  },
  logout () {
    try {
      this.uid = null
      this.intercomUserInited = false
      return intercomPlugin.shutdown()
    } catch (err) {
      console.error(err)
    }
  }
}
Object.assign(intercomPlugin, intercomPluginMixin)
export default ({ Vue }) => {
  Vue.prototype.$intercom = intercomPlugin
}

export { intercomPlugin as intercom }
