const path = require('path')
const Dotenv = require('dotenv-webpack')

const dev = require('./config/dev.env')
const staging = require('./config/staging.env')
const prod = require('./config/prod.env')
const preprod = require('./config/preprod.env')

function getConfig (ctx) {
  const specifiedEnvironment = process.env.TARGET_ENV
  if (specifiedEnvironment === 'prod') {
    return prod
  } else if (specifiedEnvironment === 'preprod') {
    return preprod
  } else if (specifiedEnvironment === 'staging') {
    return staging
  } else if (specifiedEnvironment === 'dev') {
    return dev
  }

  if (ctx.prod) {
    return prod
  }

  return dev
}

module.exports = function (ctx) {
  return {
    // app plugins (/src/plugins)
    plugins: [
      'vuelidate',
      'firebase',
      'vue-croppie',
      'vue-sanitize',
      'axios'
    ],
    css: [
      'app.styl'
    ],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      'material-icons' // optional, you are not bound to it
      // 'ionicons',
      // 'mdi',
      // 'fontawesome'
    ],
    supportIE: false,
    build: {
      scopeHoisting: true,
      vueRouterMode: 'history',
      publicPath: '/portal/',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/
        })
        cfg.resolve.alias = {
          ...cfg.resolve.alias,
          // Adds aliases for non-default folders in src
          config: path.resolve(__dirname, './src/config'),
          helpers: path.resolve(__dirname, './src/helpers'),
          shared: path.resolve(__dirname, './src/shared'),
          statics: path.resolve(__dirname, './src/statics'),
          store: path.resolve(__dirname, './src/store'),
          plugins: path.resolve(__dirname, './src/plugins'),
          containers: path.resolve(__dirname, './src/containers'),
          mixins: path.resolve(__dirname, './src/mixins')
        }
        cfg.plugins.push(new Dotenv({
          systemvars: true
        }))
      },
      env: getConfig(ctx)
    },
    devServer: {
      // https: true,
      // port: 8080,
      open: true // opens browser window automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      components: [
        'QLayout',
        'QLayoutHeader',
        'QLayoutFooter',
        'QLayoutDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QIcon',
        'QTooltip',
        'QList',
        'QListHeader',
        'QItem',
        'QItemMain',
        'QItemSide',
        'QField',
        'QInput',
        'QDatetime',
        'QDatetimePicker',
        'QPopover',
        'QTable',
        'QTh',
        'QTr',
        'QTd',
        'QTableColumns',
        'QInnerLoading',
        'QSpinner',
        'QSlideTransition',
        'QCheckbox',
        'QChipsInput',
        'QAutocomplete',
        'QUploader',
        'QCard',
        'QCardSeparator',
        'QSelect',
        'QSearch',
        'QCollapsible',
        'QTabs',
        'QTab',
        'QTabPane',
        'QOptionGroup',
        'QDialog',
        'QRadio',
        'QBtnToggle',
        'QInfiniteScroll',
        'QPopupEdit'
      ],
      directives: [
        'Ripple'
      ],
      // Quasar plugins
      plugins: [
        'Notify',
        'Dialog',
        'Loading'
      ]
      // iconSet: ctx.theme.mat ? 'material-icons' : 'ionicons'
      // i18n: 'de' // Quasar language
    },
    // animations: 'all' --- includes all animations
    animations: [
    ],
    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {},
      manifest: {
        // name: 'Quasar App',
        // short_name: 'Quasar-PWA',
        // description: 'Best PWA App in town!',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      // bundler: 'builder', // or 'packager'
      extendWebpack (cfg) {
        // do something with Electron process Webpack cfg
      },
      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      },
      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'quasar-app'
      }
    }
  }
}
