# Quasar App

### Install development env

Based on quasar js requirements, advanced guide can be found [here](https://quasar-framework.org/guide/app-installation.html)

1. Install `node` version lts/carbon `v8.x` which should come with `npm`. 
Information on how to install: [node](https://nodejs.org/en/)

2. Install `quasar-cli` using npm globally:

    npm install -g quasar-cli

3. Install globally `vue-cli`:
    
    npm install -g vue-cli

4. Install project dependencies:

    npm install

### Building and running

Running locally:

    quasar dev
    
Generate a build:

    quasar build
    
By default, `quasar dev` uses development environment config, while `quasar build` uses the production one (i.e. pointing to production Firebase backend). The target environment can be specified by passing `TARGET_ENV` variable to the CLI command set either to `dev` or `prod`, e.g.:

    TARGET_ENV=dev quasar build