const prod = require('./prod.env')
const merge = require('webpack-merge')

module.exports = merge(prod, {
  firebaseConfig: {
    apiKey: '"AIzaSyBq_8Y_XIpclM5q3awMz6AwC-1cAD_VPYI"',
    authDomain: '"qdooz-app-dev.firebaseapp.com"',
    databaseURL: '"https://qdooz-app-dev.firebaseio.com"',
    projectId: '"qdooz-app-dev"',
    storageBucket: '"qdooz-app-dev.appspot.com"',
    messagingSenderId: '"808548429428"',
    cloudFunctionsURL: '"https://us-central1-qdooz-app-dev.cloudfunctions.net"'
  },
  wistia: {
    apiKey: "'d83f5396aba97f946b008d60642579a28347efabf382c5682f00521750efc7bd'",
    apiUrl: "'https://api.wistia.com/v1/'",
    projectId: "'bvrgljdfyo'",
    uploadUrl: "'https://upload.wistia.com'",
    uploadToken: "'115789f483febee954e91f260eb80e4692b112a020baab17dc6390a9bf7a6fc8'",
    videoJSONUrl: "'https://fast.wistia.com/embed/medias/'"
  },
  webapp: {
    url: '"https://qdooz-app-dev.firebaseapp.com/webpreview"',
    content: '"https://qdooz-app-dev.firebaseapp.com/portal/content"',
    webUrl: '"https://qdooz-webapp-dev.firebaseapp.com"',
    preview: '"preview/article"'
  },
  intercom: {
    appId: "'v0vevbsc'"
  },
  slack: {
    portalAppId: "'BTQC5QW73/9OtnjrrFGpHD9NDSJP8I1rCI'"
  },
  sentry: {
    env: "'Development'"
  }
})
