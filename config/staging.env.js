const prod = require('./prod.env')
const merge = require('webpack-merge')

module.exports = merge(prod, {
  firebaseConfig: {
    apiKey: '"AIzaSyBo33WAlgKTudYU2weYIt_VDjm2rYgoZyk"',
    authDomain: '"qdooz-staging.firebaseapp.com"',
    databaseURL: '"https://qdooz-staging.firebaseio.com"',
    projectId: '"qdooz-staging"',
    storageBucket: '"qdooz-staging.appspot.com"',
    messagingSenderId: '"305779965443"',
    cloudFunctionsURL: '"https://us-central1-qdooz-staging.cloudfunctions.net"',
    appId: '"1:305779965443:web:6d3ebf326eafb404"'
  },
  webapp: {
    url: '"https://qdooz-staging-portal.firebaseapp.com/webpreview"',
    content: '"https://qdooz-staging-portal.firebaseapp.com/portal/content"',
    webUrl: '"https://qdooz-staging-webapp.firebaseapp.com"',
    preview: '"preview/article"'
  },
  intercom: {
    appId: "'v0vevbsc'"
  },
  slack: {
    portalAppId: "'BTQC5QW73/9OtnjrrFGpHD9NDSJP8I1rCI'"
  },
  sentry: {
    env: "'Staging'"
  }
})
