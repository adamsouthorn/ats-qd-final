module.exports = {
  firebaseConfig: {
    apiKey: '"AIzaSyAmHNwbub4mWsES8APw8WlXrBuEvScQsMc"',
    authDomain: '"qdooz-app.firebaseapp.com"',
    databaseURL: '"https://qdooz-app.firebaseio.com"',
    projectId: '"qdooz-app"',
    storageBucket: '"qdooz-app.appspot.com"',
    messagingSenderId: '"485373039467"',
    cloudFunctionsURL: '"https://us-central1-qdooz-app.cloudfunctions.net"'
  },
  wistia: {
    apiKey: "'d83f5396aba97f946b008d60642579a28347efabf382c5682f00521750efc7bd'",
    apiUrl: "'https://api.wistia.com/v1/'",
    projectId: "'s7xesa2mgw'",
    uploadUrl: "'https://upload.wistia.com'",
    uploadToken: "'115789f483febee954e91f260eb80e4692b112a020baab17dc6390a9bf7a6fc8'",
    videoJSONUrl: "'https://fast.wistia.com/embed/medias/'"
  },
  api: {
    version: '"v1"'
  },
  storage: {
    version: '"v1"'
  },
  firestore: {
    version: '"v1"'
  },
  webapp: {
    url: '"https://portal.qdooz.com/webpreview"',
    content: '"https://portal.qdooz.com/portal/content"',
    webUrl: '"https://qdooz.com"',
    preview: '"preview/article"'
  },
  intercom: {
    appId: "'rms15slv'"
  },
  slack: {
    portalAppId: "'BTQC5QW73/9OtnjrrFGpHD9NDSJP8I1rCI'"
  },
  sentry: {
    dsn: "'https://c1c982f34a844b46ab2b365d3ef12c3a@sentry.io/2440565'",
    env: "'Production'"
  }
}
