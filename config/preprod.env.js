const prod = require('./prod.env')
const merge = require('webpack-merge')

module.exports = merge(prod, {
  firebaseConfig: {
    apiKey: '"AIzaSyDqrSl9LJeeDI8IGK4Pk54jkcYxe4B8A8c"',
    authDomain: '"qdooz-testing.firebaseapp.com"',
    databaseURL: '"https://qdooz-testing.firebaseio.com"',
    projectId: '"qdooz-testing"',
    storageBucket: '"qdooz-testing.appspot.com"',
    messagingSenderId: '"305779965443"',
    appId: '"1:305779965443:web:6d3ebf326eafb404"',
    webAppUrl: '"https://qdooz-testing-webapp.firebaseapp.com"'
  },
  webapp: {
    url: '"https://qdooz-testing-portal.firebaseapp.com/webpreview"',
    content: '"https://qdooz-testing-portal.firebaseapp.com/portal/content"',
    webUrl: '"https://qdooz-testing-webapp.firebaseapp.com"',
    preview: '"preview/article"'
  },
  intercom: {
    appId: "'v0vevbsc'"
  },
  slack: {
    portalAppId: "'BTQC5QW73/9OtnjrrFGpHD9NDSJP8I1rCI'"
  },
  sentry: {
    env: "'Preproduction'"
  }
})
