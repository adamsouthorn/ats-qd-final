const faker = require('faker')

const createUserData = () => ({
  email: `test_${faker.internet.email()}`,
  password: 'password'
})

const mockedUser = createUserData()

const getRandomInt = (max) => {
  return Math.floor(Math.random() * Math.floor(max))
}

module.exports = {
  mockedUser,
  createUserData,
  getRandomInt,
  defaultPauseTime: 500,
  defaultWaitTime: 20000
}
