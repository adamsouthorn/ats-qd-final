const { defaultWaitTime } = require('../../setup')

const authenticationPageCommand = {
  signIn () {
    return this.waitForElementVisible('[data-test-id="sign-in-container"]', defaultWaitTime)
      .setValue('input[type=email]', process.env.CONTENT_PROVIDER_EMAIL)
      .setValue('input[type=password]', process.env.CONTENT_PROVIDER_PASSWORD)
      .click('[data-test-id="sign-in-btn"]')
  }
}

module.exports = {
  url: function () { return (this.api && this.api.launchUrl && `${this.api.launchUrl}`) || 'http://localhost:8080' },
  commands: authenticationPageCommand
}
