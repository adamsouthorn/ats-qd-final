const goToPageCommand = {
  profilePage () {
    return this.click('[data-test-id="header-link-profile"]')
  }
}

module.exports = {
  url: function () { return (this.api && this.api.launchUrl && `${this.api.launchUrl}/login`) || 'http://localhost:8080/login' },
  commands: goToPageCommand
}
