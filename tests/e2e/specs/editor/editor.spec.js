const faker = require('faker')
const { defaultPauseTime } = require('../../setup')
const DEFAULT_WAIT_TIME = 30000
const ARTICLE_TITLE = `Test agent - ${Math.random()}`
const INTRO_TEXT = faker.lorem.sentence(17)
module.exports = {

  // Create New Content
  'Sign in, create new content, try typing fast in new text box': function (browser) {
    const authPage = browser.page.auth.authentication()
    authPage.navigate(`${browser.launchUrl}/content`).signIn()

    browser
      .waitForElementVisible('body')
      .waitForElementVisible('[data-test-id="navigator-content"]', DEFAULT_WAIT_TIME)
      .click('[data-test-id="navigator-content"]')
      .waitForElementVisible('[data-test-id="content-list-page"]', DEFAULT_WAIT_TIME)
      .waitForElementVisible('[data-test-id="add-new-content"]', DEFAULT_WAIT_TIME)
      .click('[data-test-id="add-new-content"] .q-btn')
      .waitForElementVisible('body')
      .waitForElementVisible('[data-test-id="content-edit-page"]', DEFAULT_WAIT_TIME)
      .waitForElementVisible('[data-test-id="content-title-input"]', DEFAULT_WAIT_TIME)
      .moveToElement('[data-test-id="content-title-input"]', 0, 100)
      .waitForElementVisible('[data-test-id="content-title-input"] input[type=text]', DEFAULT_WAIT_TIME)
      .setValue('[data-test-id="content-title-input"] input[type=text]', ARTICLE_TITLE)
      .waitForElementVisible('[data-test-id="content-intro-text"] p', DEFAULT_WAIT_TIME)
      .moveToElement('[data-test-id="content-intro-text"] p', 0, 100)
      .click('[data-test-id="content-intro-text"] p')
      .keys(INTRO_TEXT)
      .assert.containsText('[data-test-id="content-intro-text"] p', INTRO_TEXT)
      .waitForElementVisible('[data-test-id="delete-article"]', DEFAULT_WAIT_TIME)
      .click('[data-test-id="delete-article"]')
      .waitForElementVisible('.modal-message', DEFAULT_WAIT_TIME)
      .pause(defaultPauseTime)
      .keys('\uE00D')
      .pause(defaultPauseTime)
      .end()
  }
}
