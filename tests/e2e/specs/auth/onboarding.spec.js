
const faker = require('faker')
const { mockedUser, defaultPauseTime } = require('../../setup')
const DEFAULT_WAIT_TIME = 30000
module.exports = {

  // Create New User
  'Create New User': function (browser) {
    browser
      .url(browser.launchUrl)
      .waitForElementVisible('body')
      .waitForElementVisible('[data-test-id="create-account-btn"] .q-btn', DEFAULT_WAIT_TIME)
      .click('[data-test-id="create-account-btn"] .q-btn')
      .waitForElementVisible('input[type=text]', DEFAULT_WAIT_TIME)
      .setValue('input[type=text]', faker.name.firstName())
      .setValue('input[type=email]', mockedUser.email)
      .setValue('input[type=password]', mockedUser.password)
      .click('[data-test-id="sign-up-btn"] .q-btn')
      .pause(defaultPauseTime)
      .waitForElementVisible('body')
      .waitForElementVisible('[data-test-id="user-page"] input[type=text]', DEFAULT_WAIT_TIME)
      .end()
  }
}
