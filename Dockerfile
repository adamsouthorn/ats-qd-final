# Base Environment
FROM node:carbon-alpine as base

ARG commit_sha
ARG environment="dev"
ARG version="development"

ENV COMMIT_SHA=${commit_sha}
ENV TARGET_ENV=${environment}
ENV VERSION_NUMBER=${version}

WORKDIR /qdooz
COPY package*.json ./
RUN npm ci

# Hot-reload Environment
FROM base as qdooz-portal-hot-reload
CMD [ "npm", "start" ]

# Setup Build Environment
FROM base as qdooz-portal-bundled
COPY . .
RUN [ "npm", "run", "build" ]

# Bundled Environment
FROM nginx:stable-alpine as qdooz-portal
COPY --from=qdooz-portal-bundled /qdooz/dist/spa-mat /app/portal
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD [ "nginx", "-g", "daemon off;" ]
